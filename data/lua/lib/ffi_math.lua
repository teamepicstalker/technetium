-- Custom math library for 3D computations

local ffi = require("ffi")
ffi.cdef([[
typedef struct { float x, y, z; } vec2_t;
typedef struct { float x, y, z; } vec3_t;
typedef struct { float x, y, z, w; } quat_t;
typedef union {
	float m[16];
	struct {	// row-major
		
		float r00, r01, r02, r03;
		float r10, r11, r12, r13;
		float r20, r21, r22, r23;
		float r30, r31, r32, r33;
	};
	struct {	// column-major
		float c00, c10, c20, c30;
		float c01, c11, c21, c31;
		float c02, c12, c22, c32;
		float c03, c13, c23, c33;
	};
} mat4_t;

double round(double);
]])


-- vec2
local vec2_mt = { __index = {} }
local vec2 = vec2_mt.__index

function vec2_mt.__call(self,x,y)
	self.x = x or 0
	self.y = y or 0
	return self
end

function vec2.add(self,x,y)
	return ffi.new("vec2_t",self.x+x,self.y+y)
end

function vec2.sub(self,x,y)
	return ffi.new("vec2_t",self.x-x,self.y-y)
end

function vec2.mul(self,x,y)
	return ffi.new("vec2_t",self.x*x,self.y*y)
end

function vec2.div(self,x,y)
	return ffi.new("vec2_t",self.x/x,self.y/y)
end

function vec2.clone(self)
	return ffi.new("vec2_t",self.x,self.y)
end

function vec2.unpack(self)
	return self.x,self.y
end

-- vec3
local vec3_mt = { __index = {} }
local vec3 = vec3_mt.__index

function vec3_mt.__call(self,x,y,z)
	self.x = x or 0
	self.y = y or 0
	self.z = z or 0
	return self
end

--@usage: a:add(b:unpack())
--@returns new vec3_t
function vec3.add(self,x,y,z)
	return ffi.new("vec3_t",self.x+x,self.y+y,self.z+z)
end
vec3_mt.__add = function(a,b) return vec3.add(a,b:unpack()) end

--@usage:  a:sub(b:unpack())
function vec3.sub(self,x,y,z)
	return ffi.new("vec3_t",self.x-x,self.y-y,self.z-z)
end
vec3_mt.__sub = function(a,b) return vec3.sub(a,b:unpack()) end

--@usage: a:mul(b:unpack())
function vec3.mul(self,x,y,z)
	return ffi.new("vec3_t",self.x*x,self.y*y,self.z*z)
end
vec3_mt.__mul = function(a,b) return vec3.mul(a,b:unpack()) end

--@usage: a:div(b:unpack())
function vec3.div(self,x,y,z)
	return ffi.new("vec3_t",self.x/x,self.y/y,self.z/z)
end
vec3_mt.__div = function(a,b) return vec3.div(a,b:unpack()) end

function vec3.cross(self,x,y,z)
	return ffi.new("vec3_t",self.y*z - self.z*y, self.z*x - self.x*z, self.x*y - self.y*x)
end

function vec3.dot(self,x,y,z)
	return self.x*x + self.y*y + self.z*z
end

function vec3.length(self)
	return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
end

function vec3.length2(self)
	return self.x * self.x + self.y * self.y + self.z * self.z
end

function vec3.distance(self,x,y,z)
	local dx = self.x-x
	local dy = self.y-y
	local dz = self.z-z
	return math.sqrt(dx * dx + dy * dy + dz * dz)
end

function vec3.distance2(self,x,y,z)
	local dx = self.x-x
	local dy = self.y-y
	local dz = self.z-z
	return dx * dx + dy * dy + dz * dz
end

function vec3.scale(self,s)
	return ffi.new("vec3_t",self.x*s,self.y*s,self.z*s)
end

function vec3.normalize(self)
	if self:is_zero() then
		return ffi.new("vec3_t")
	end
	local len = self:length()
	if (len > 0) then
		return self:scale(1 / len)
	end
	return ffi.new("vec3_t")
end

--@usage: local b = a:clone()
function vec3.clone(self)
	return ffi.new("vec3_t",self.x,self.y,self.z)
end

function vec3.unpack(self)
	return self.x,self.y,self.z
end

function vec3.is_zero(self)
	return self.x == 0 and self.y == 0 and self.z == 0
end

-- quat
local quat_mt = { __index = {} }
local quat = quat_mt.__index

function quat_mt.__call(self,x,y,z)
	self.x = x or 0
	self.y = y or 0
	self.z = z or 0
	return self
end

function quat.add(self,x,y,z,w)
	return ffi.new("quat_t",self.x+x,self.y+y,self.z+z,self.w+w)
end
quat_mt.__add = function(a,b) return quat.add(a,b:unpack()) end

function quat.sub(self,x,y,z,w)
	return ffi.new("quat_t",self.x-x,self.y-y,self.z-z,self.w-w)
end
quat_mt.__sub = function(a,b) return quat.sub(a,b:unpack()) end

function quat.mul(self,x,y,z,w)
	return ffi.new("quat_t",self.x*w+self.w*x+self.y*z-self.z*y,self.y*w+self.w*y+self.z*x-self.x*z,self.z*w+self.w*z+self.x*y-self.y*x,self.w*w-self.x*x-self.y*y-self.z*z)
end
quat_mt.__mul = function(a,b) return quat.mul(a,b:unpack()) end

function quat.scale(self,s)
	return ffi.new("quat_t",self.x*s,self.y*s,self.z*s,self.w*s)
end

function quat.clone(self)
	return ffi.new("quat_t",self.x,self.y,self.z,self.w)
end

function quat.unpack(self)
	return self.x,self.y,self.z,self.w
end

-- mat4
local mat4_mt = { __index = {} }
local mat4 = mat4_mt.__index

function mat4_mt.__call(self,r00,r01,r02,r03,r10,r11,r12,r13,r20,r21,r22,r23,r30,r31,r32,r33)
	self.r00 = r00 or 1
	self.r01 = r01 or 0
	self.r02 = r02 or 0
	self.r03 = r03 or 0
	self.r10 = r10 or 0
	self.r11 = r11 or 1
	self.r12 = r12 or 0
	self.r13 = r13 or 0
	self.r20 = r20 or 0
	self.r21 = r21 or 0
	self.r22 = r22 or 1
	self.r23 = r23 or 0
	self.r30 = r30 or 0
	self.r31 = r31 or 0
	self.r32 = r32 or 0
	self.r33 = r33 or 1
	return self
end

function mat4.identity(self)
	self.r00 = 1
	self.r01 = 0
	self.r02 = 0
	self.r03 = 0
	self.r10 = 0
	self.r11 = 1
	self.r12 = 0
	self.r13 = 0
	self.r20 = 0
	self.r21 = 0
	self.r22 = 1
	self.r23 = 0
	self.r30 = 0
	self.r31 = 0
	self.r32 = 0
	self.r33 = 1
	return self
end

function mat4.mul(self,r00,r01,r02,r03,r10,r11,r12,r13,r20,r21,r22,r23,r30,r31,r32,r33)
	local out = ffi.new("mat4_t")
	out.r00  = r00  * self.r00 + r01  * self.r10 + r02  * self.r20  + r03  * self.r30
	out.r01  = r00  * self.r01 + r01  * self.r11 + r02  * self.r21 + r03  * self.r31
	out.r02  = r00  * self.r02 + r01  * self.r12 + r02  * self.r22 + r03  * self.r32
	out.r03  = r00  * self.r03 + r01  * self.r13 + r02  * self.r23 + r03  * self.r33
	out.r10  = r10  * self.r00 + r11  * self.r10 + r12  * self.r20  + r13  * self.r30
	out.r11  = r10  * self.r01 + r11  * self.r11 + r12  * self.r21 + r13  * self.r31
	out.r12  = r10  * self.r02 + r11  * self.r12 + r12  * self.r22 + r13  * self.r32
	out.r13  = r10  * self.r03 + r11  * self.r13 + r12  * self.r23 + r13  * self.r33
	out.r20  = r20  * self.r00 + r21 * self.r10 + r22 * self.r20  + r23 * self.r30
	out.r21 = r20  * self.r01 + r21 * self.r11 + r22 * self.r21 + r23 * self.r31
	out.r22 = r20  * self.r02 + r21 * self.r12 + r22 * self.r22 + r23 * self.r32
	out.r23 = r20  * self.r03 + r21 * self.r13 + r22 * self.r23 + r23 * self.r33
	out.r30 = r30 * self.r00 + r31 * self.r10 + r32 * self.r20  + r33 * self.r30
	out.r31 = r30 * self.r01 + r31 * self.r11 + r32 * self.r21 + r33 * self.r31
	out.r32 = r30 * self.r02 + r31 * self.r12 + r32 * self.r22 + r33 * self.r32
	out.r33 = r30 * self.r03 + r31 * self.r13 + r32 * self.r23 + r33 * self.r33
	return out
end
mat4_mt.__mul = function (a,b) return mat4.mul(a,b:unpack()) end

function mat4.translation(self,x,y,z)
	local b = ffi.new("mat4_t"):identity()
	b.r30 = x
	b.r31 = y
	b.r32 = z
	return b
end

function mat4.translate(self,x,y,z)
    self.r30 = self.r00 * x + self.r10 * y + self.r20 * z + self.r30
    self.r31 = self.r01 * x + self.r11 * y + self.r21 * z + self.r31
    self.r32 = self.r02 * x + self.r12 * y + self.r22 * z + self.r32
    self.r33 = self.r03 * x + self.r13 * y + self.r23 * z + self.r33
    return self
end

function mat4.transpose(self)
	return ffi.new("mat4_t")(self.r00,self.r10,self.r20,self.r30,self.r01,self.r11,self.r21,self.r31,self.r02,self.r12,self.r22,self.r32,self.r03,self.r13,self.r23,self.r33)
end

function mat4.scale(self,x,y,z)
	self.r00 = x
	self.r11 = y
	self.r22 = z
	return self
end

function mat4.from_ortho(self,left,right,bottom,top,near,far,offset,is_homogeneousNDC,is_right_handed)
	local aa = 2 / (right - left )
	local bb = 2 / (top - bottom)
	local cc = (is_homogeneousNDC and 2 or 1) / (far - near)
	local dd = (left + right) / (left - right)
	local ee = (top + bottom) / (bottom - top)
	local ff = is_homogeneousNDC and (near + far) / (near - far) or (near / (near - far))
	
	self.r00 = aa
	self.r11 = bb
	self.r22 = is_right_handed and -cc or cc
	self.r30 = dd + offset
	self.r31 = ee
	self.r32 = ff
	self.r33 = 1
	
	return self
end

function mat4.look_at(self,eye,center,up,is_right_handed)
    local zaxis = is_right_handed and (eye - center) or (center - eye):normalize()
    local xaxis = up:cross(zaxis:unpack()):normalize()
    local yaxis = zaxis:cross(xaxis:unpack())
    self.r00, self.r01, self.r02, self.r03 = xaxis.x, yaxis.x, zaxis.x, 0
    self.r10, self.r11, self.r12, self.r13 = xaxis.y, yaxis.y, zaxis.y, 0
    self.r20, self.r21, self.r22, self.r23 = xaxis.z, yaxis.z, zaxis.z, 0
    self.r30, self.r31, self.r32, self.r33 = -xaxis:dot(eye:unpack()), -yaxis:dot(eye:unpack()), -zaxis:dot(eye:unpack()), 1
    return self
end

function mat4.from_perspective(self,fovy,aspect,near,far,is_homogeneousNDC,is_right_handed)
	local x,y = 0,0
	local height = 1/math.tan(math.rad(fovy)*0.5)
	local width = height * 1/aspect
	local diff = far-near
	local aa = is_homogeneousNDC and (far + near)/diff or far/diff
	local bb = is_homogeneousNDC and (2*far*near)/diff or near*aa
	
	self:identity()
	
	self.r00 = width
	self.r11 = height
	self.r20 = is_right_handed and x or -x
	self.r21 = is_right_handed and y or -y
	self.r22 = is_right_handed and -aa or aa
	self.r23 = is_right_handed and -1 or 1
	self.r32 = -bb
	
	return self
end

function mat4.mul_pos(self,x,y,z)
	local out = ffi.new("vec3_t",self.r00*x+self.r10*y+self.r20*z+self.r30, self.r01*x+self.r11*y+self.r21*z+self.r31, self.r02*x+self.r12*y+self.r22*z+self.r32)
	local w = self.r03*x+self.r13*y+self.r23*z+self.r33
	if not (w == 0 or w == 1) then
		return out(out.x/w,out.y/w,out.z/w)
	end
	return out
end

function mat4.mul_dir(self,x,y,z)
	local out = ffi.new("vec3_t",self.r00*x+self.r10*y+self.r20*z, self.r01*x+self.r11*y+self.r21*z, self.r02*x+self.r12*y+self.r22*z)
	if not (w == 0 or w == 1) then
		return out(out.x/w,out.y/w,out.z/w)
	end
	return out
end

function mat4.rotateX(self,ax)
	local sx = math.sin(ax)
	local cx = math.cos(ax)
	self.r00 = 1
	self.r11 = cx
	self.r12 = -sx
	self.r21 = sx
	self.r22 = cx
	self.r33 = 1
	return self
end

function mat4.rotateY(self,ay)
	local sy = math.sin(ay)
	local cy = math.cos(ay)
	self.r00 = cy
	self.r02 = sy
	self.r11 = 1
	self.r20 = -sy
	self.r22 = cy
	self.r33 = 1
	return self
end

function mat4.rotateZ(self,az)
	local sz = math.sin(az)
	local cz = math.cos(az)
	self.r00 = cz
	self.r01 = -sz
	self.r10 = sz
	self.r11 = cz
	self.r22 = 1
	self.r33 = 1
	return self
end

function mat4.rotateXY(self,ax,ay)
	local sx = math.sin(ax)
	local cx = math.cos(ax)
	local sy = math.sin(ay)
	local cy = math.cos(ay)
	self.r00 = cy
	self.r02 = sy
	self.r10 = sx*sy
	self.r11 = cx
	self.r12 = -sx*cy
	self.r20 = -cx*sy
	self.r21 = sx
	self.r22 = cx*cy
	self.r33 = 1
	return self
end

function mat4.rotateXYZ(self,ax,ay,az)
	local sx = math.sin(ax)
	local cx = math.cos(ax)
	local sy = math.sin(ay)
	local cy = math.cos(ay)
	local sz = math.sin(az)
	local cz = math.cos(az)
	self.r00 = cy*cz
	self.r01 = -cy*sz
	self.r02 = sy
	self.r10 = cz*sx*sy + cx*sz
	self.r11 = cx*cz - sx*sy*sz
	self.r12 = -cy*sx
	self.r20 = -cx*cz*sy + sx*sz
	self.r21 = cz*sx + cx*sy*sz
	self.r22 = cx*cy
	self.r33 = 1
	return self
end

function mat4.rotateZYX(self,az,ay,ax)
	local sx = math.sin(ax)
	local cx = math.cos(ax)
	local sy = math.sin(ay)
	local cy = math.cos(ay)
	local sz = math.sin(az)
	local cz = math.cos(az)
	self.r00 = cy*cz
	self.r01 = cz*sx*sy-cx*sz
	self.r02 = cx*cz*sy+sx*sz
	self.r10 = cy*sz
	self.r11 = cx*cz + sx*sy*sz
	self.r12 = -cz*sx + cx*sy*sz
	self.r20 = -sy
	self.r21 = cy*sx
	self.r22 = cx*cy
	self.r33 = 1
	return self
end

function mat4.rotation(angle_in_rad,axis)
	local c = math.cos(angle_in_rad)
	local s = math.sin(angle_in_rad)
	local n = axis:normalize()
	local x,y,z = n.x,n.y,n.z
	local out = ffi.new("mat4_t")
	out.r00 = c + x*x*(1-c)
	out.r01 = x*y*(1-c) - z*s
	out.r02 = x*z*(1-c) + y*s
	out.r10 = y*x*(1-c) + z*s
	out.r11 = c + y*y*(1-c)
	out.r12 = y*z*(1-c) - x*s
	out.r20 = z*x*(1-c) - y*s
	out.r21 = z*y*(1-c) + x*s
	out.r22 = c + z*z*(1-c)
	out.r33 = 1
	return out
end

--@usage: local norm_mtx = mdl_mtx:clone():inverse()
function mat4.inverse(self)
	local xx = self.r00
	local xy = self.r01
	local xz = self.r02
	local xw = self.r03
	local yx = self.r10
	local yy = self.r11
	local yz = self.r12
	local yw = self.r13
	local zx = self.r20
	local zy = self.r21
	local zz = self.r22
	local zw = self.r23
	local wx = self.r30
	local wy = self.r31
	local wz = self.r32
	local ww = self.r33
	local det = 0
	det = det + xx * (yy*(zz*ww - zw*wz) - yz*(zy*ww - zw*wy) + yw*(zy*wz - zz*wy) )
	det = det - xy * (yx*(zz*ww - zw*wz) - yz*(zx*ww - zw*wx) + yw*(zx*wz - zz*wx) )
	det = det + xz * (yx*(zy*ww - zw*wy) - yy*(zx*ww - zw*wx) + yw*(zx*wy - zy*wx) )
	det = det - xw * (yx*(zy*wz - zz*wy) - yy*(zx*wz - zz*wx) + yz*(zx*wy - zy*wx) )
	local invDet = 1/det
	local out = ffi.new("mat4_t")
	out.r00 = (yy*(zz*ww - wz*zw) - yz*(zy*ww - wy*zw) + yw*(zy*wz - wy*zz) ) * invDet
	out.r01 = -(xy*(zz*ww - wz*zw) - xz*(zy*ww - wy*zw) + xw*(zy*wz - wy*zz) ) * invDet
	out.r02 = (xy*(yz*ww - wz*yw) - xz*(yy*ww - wy*yw) + xw*(yy*wz - wy*yz) ) * invDet
	out.r03 = -(xy*(yz*zw - zz*yw) - xz*(yy*zw - zy*yw) + xw*(yy*zz - zy*yz) ) * invDet
	out.r10 = -(yx*(zz*ww - wz*zw) - yz*(zx*ww - wx*zw) + yw*(zx*wz - wx*zz) ) * invDet
	out.r11 = (xx*(zz*ww - wz*zw) - xz*(zx*ww - wx*zw) + xw*(zx*wz - wx*zz) ) * invDet
	out.r12 = -(xx*(yz*ww - wz*yw) - xz*(yx*ww - wx*yw) + xw*(yx*wz - wx*yz) ) * invDet
	out.r13 = (xx*(yz*zw - zz*yw) - xz*(yx*zw - zx*yw) + xw*(yx*zz - zx*yz) ) * invDet
	out.r20 = (yx*(zy*ww - wy*zw) - yy*(zx*ww - wx*zw) + yw*(zx*wy - wx*zy) ) * invDet
	out.r21 = -(xx*(zy*ww - wy*zw) - xy*(zx*ww - wx*zw) + xw*(zx*wy - wx*zy) ) * invDet
	out.r22 = (xx*(yy*ww - wy*yw) - xy*(yx*ww - wx*yw) + xw*(yx*wy - wx*yy) ) * invDet
	out.r23 = -(xx*(yy*zw - zy*yw) - xy*(yx*zw - zx*yw) + xw*(yx*zy - zx*yy) ) * invDet
	out.r30 = -(yx*(zy*wz - wy*zz) - yy*(zx*wz - wx*zz) + yz*(zx*wy - wx*zy) ) * invDet
	out.r31 = (xx*(zy*wz - wy*zz) - xy*(zx*wz - wx*zz) + xz*(zx*wy - wx*zy) ) * invDet
	out.r32 = -(xx*(yy*wz - wy*yz) - xy*(yx*wz - wx*yz) + xz*(yx*wy - wx*yy) ) * invDet
	out.r33 = (xx*(yy*zz - zy*yz) - xy*(yx*zz - zx*yz) + xz*(yx*zy - zx*yy) ) * invDet
	return out
end

function mat4.clone(self)
	local out = ffi.new("mat4_t")
	out(self.r00,self.r01,self.r02,self.r03,self.r10,self.r11,self.r12,self.r13,self.r20,self.r21,self.r22,self.r23,self.r30,self.r31,self.r32,self.r33)
	return out
end

function mat4.unpack(self)
	return self.r00,self.r01,self.r02,self.r03,self.r10,self.r11,self.r12,self.r13,self.r20,self.r21,self.r22,self.r23,self.r30,self.r31,self.r32,self.r33
end

-- assign metatables to ctypes
ffi.metatype("vec2_t",vec2_mt)
ffi.metatype("vec3_t",vec3_mt)
ffi.metatype("quat_t",quat_mt)
ffi.metatype("mat4_t",mat4_mt)