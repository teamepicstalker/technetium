-------------------------------------------------------------
-- Initialize third-party libraries into global namespaces
-------------------------------------------------------------
require("ffi_math")
json			= require("json")
glfw 			= require("ffi_glfw")										; if not (glfw) then error("Failed to load glfw") end
physfs			= require("ffi_physfs")										; if not (physfs) then error("Failed to load physfs") end
--lanes			= require("lanes").configure()
Class			= require("class")
time 			= require("time")
