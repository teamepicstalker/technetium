function escape_lua_pattern(s)
    return (s:gsub(".",{
		["^"] = "%^",
		["$"] = "%$",
		["("] = "%(",
		[")"] = "%)",
		["%"] = "%%",
		["."] = "%.",
		["["] = "%[",
		["]"] = "%]",
		["*"] = "%*",
		["+"] = "%+",
		["-"] = "%-",
		["?"] = "%?",
		["\0"] = "%z"
	}))
end

function str_explode(str,sep,plain)
	if not (sep ~= "" and string.find(str,sep,1,plain)) then
		return { str }
	end
	local t = {}
	--for s in str:gsplit(sep,plain) do 
	for s in str:gmatch("([^"..escape_lua_pattern(sep).."]+)") do
		table.insert(t,trim(s))
	end
	return t
end

function trim(s)
	return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

function get_ext(s)
	local ext = string.gsub(s,"(.*)%.","")
	if (s == ext) then -- no ext
		return ""
	end
	return ext
end

function get_path(s)
    return s:match("(.*[\\/])")
end

function trim_path(s,sep)
	local ret = s:gsub("(.*[\\/])","")
	return ret
end

function trim_ext(s)
	local ret = s:gsub("(%..*)","")
	return ret
end

function table_copy(dest,src)
	local stack = {}
	local node1 = dest
	local node2 = src
	while (true) do
		for k,v in pairs(node2) do
			if type(v) == "table" then
				node1[k] = {}
				table.insert(stack,{node1[k],v})
			else
				node1[k] = v
			end
		end
		if (#stack > 0) then
			local t = stack[#stack]
			node1,node2 = t[1],t[2]
			stack[#stack] = nil
		else
			break
		end
	end
	return dest
end

function table_merge(into,from)
	local stack = {}
	local node1 = into
	local node2 = from
	while (true) do
		for k,v in pairs(node2) do
			if (type(v) == "table" and type(node1[k]) == "table") then
				table.insert(stack,{node1[k],node2[k]})
			else
				node1[k] = v
			end
		end
		if (#stack > 0) then
			local t = stack[#stack]
			node1,node2 = t[1],t[2]
			stack[#stack] = nil
		else
			break
		end
	end
	return into
end

function base64_encode(data)
    return ((data:gsub('.', function(x) 
        local r, b= '' , x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

function base64_decode(data)
	local base64 = [[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/]]
    data = string.gsub(data, '[^'..base64..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(base64:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
            return string.char(c)
    end))
end

-- MATH helpers

function float2hex (n)
    if n == 0.0 then return 0.0 end

    local sign = 0
    if n < 0.0 then
        sign = 0x80
        n = -n
    end

    local mant, expo = math.frexp(n)
    local hext = {}

    if mant ~= mant then
        hext[#hext+1] = string.char(0xFF, 0x88, 0x00, 0x00)

    elseif mant == math.huge or expo > 0x80 then
        if sign == 0 then
            hext[#hext+1] = string.char(0x7F, 0x80, 0x00, 0x00)
        else
            hext[#hext+1] = string.char(0xFF, 0x80, 0x00, 0x00)
        end

    elseif (mant == 0.0 and expo == 0) or expo < -0x7E then
        hext[#hext+1] = string.char(sign, 0x00, 0x00, 0x00)

    else
        expo = expo + 0x7E
        mant = (mant * 2.0 - 1.0) * math.ldexp(0.5, 24)
        hext[#hext+1] = string.char(sign + math.floor(expo / 0x2),
                                    (expo % 0x2) * 0x80 + math.floor(mant / 0x10000),
                                    math.floor(mant / 0x100) % 0x100,
                                    mant % 0x100)
    end

    return tonumber(string.gsub(table.concat(hext),"(.)",
                                function (c) return string.format("%02X%s",string.byte(c),"") end), 16)
end

function hex2float (c)
	if c == 0 then return 0.0 end
	c = string.gsub(string.format("%X", c),"(..)",function (x) return string.char(tonumber(x, 16)) end)

    local b1,b2,b3,b4 = string.byte(c, 1, 4)
    local sign = b1 > 0x7F
    local expo = (b1 % 0x80) * 0x2 + math.floor(b2 / 0x80)
    local mant = ((b2 % 0x80) * 0x100 + b3) * 0x100 + b4

    if sign then
        sign = -1
    else
        sign = 1
    end

    local n

    if mant == 0 and expo == 0 then
        n = sign * 0.0
    elseif expo == 0xFF then
        if mant == 0 then
            n = sign * math.huge
        else
            n = 0.0/0.0
        end
    else
        n = sign * math.ldexp(1.0 + mant / 0x800000, expo - 0x7F)
    end

    return n
end

function number2bytes(num, width)
	local function _n2b(width, num, rem)
		rem = rem * 256
		if width == 0 then return rem end
		return rem, _n2b(width-1, math.modf(num/256))
	end
	--return string.char(_n2b(width-1, math.modf(num/256)))
	return {_n2b(width-1, math.modf(num/256))}
end

function normalize(value,min,max,scale_min,scale_max)
	if (scale_min and scale_max) then
		return ( (value-min)/(max-min) ) * (scale_max-scale_min) + scale_min
	end
	return (value-min)/(max-min)
end

function denormalize(value,min,max,scale_min,scale_max)
	if (scale_min and scale_max) then
		value = (value-scale_min)/(scale_max-scale_min)
		return value*(max-min)+min
	end
	return value*(max-min)+min
end

function wrap_angle(angle)
	while (angle > math.pi) do
		angle = angle - 2*math.pi
	end
	while (angle < -math.pi) do
		angle = angle + 2*math.pi
	end
	return angle
end

function wrap_max(x,max)
	return math.fmod(max+math.fmod(x,max),max)
end

function wrap_minmax(x,min,max)
	return min + wrap_max(x-min,max-min)
end

function roundToMultiple(n,m)
	local r = n % m
	if (r == 0) then
		return n
	end
	return n + m - r
end

--- Returns `value` if it is equal or greater than |`size`|, or 0.
-- @param value
-- @param size
-- @return number
function deadzone(value, size)
	return abs(value) >= size and value or 0
end

--- Check if value is equal or greater than threshold.
-- @param value
-- @param threshold
-- @return boolean
function threshold(value, threshold)
	-- I know, it barely saves any typing at all.
	return abs(value) >= threshold
end

--- Check if value is equal or less than threshold.
-- @param value
-- @param threshold
-- @return boolean
function tolerance(value, threshold)
	-- I know, it barely saves any typing at all.
	return abs(value) <= threshold
end

--- Scales a value from one range to another.
-- @param value Input value
-- @param min_in Minimum input value
-- @param max_in Maximum input value
-- @param min_out Minimum output value
-- @param max_out Maximum output value
-- @return number
function map(value, min_in, max_in, min_out, max_out)
	return ((value) - (min_in)) * ((max_out) - (min_out)) / ((max_in) - (min_in)) + (min_out)
end

--- Linear interpolation.
-- Performs linear interpolation between 0 and 1 when `low` < `progress` < `high`.
-- @param low value to return when `progress` is 0
-- @param high value to return when `progress` is 1
-- @param progress (0-1)
-- @return number
function lerp(low, high, progress)
	return low * (1 - progress) + high * progress
end

--- Exponential decay
-- @param low initial value
-- @param high target value
-- @param rate portion of the original value remaining per second
-- @param dt time delta
-- @return number
function decay(low, high, rate, dt)
	return lerp(low, high, 1.0 - math.exp(-rate * dt))
end

--- Hermite interpolation.
-- Performs smooth Hermite interpolation between 0 and 1 when `low` < `progress` < `high`.
-- @param progress (0-1)
-- @param low value to return when `progress` is 0
-- @param high value to return when `progress` is 1
-- @return number
function smoothstep(progress, low, high)
	local t = math.clamp((progress - low) / (high - low), 0.0, 1.0)
	return t * t * (3.0 - 2.0 * t)
end