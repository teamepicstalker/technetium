-------------------------------------------
-- API
-------------------------------------------
local _fps = 0
function getFPS()
	return _fps
end
-------------------------------------------
-- Callbacks
-------------------------------------------
function application_init()

end

function application_run()
	-- framework_run callback
 	local function iterator(path,fname,fullpath)
		--print(fname)
		-- Split paths and create tables for each directory
		local node = _G
		local namespace = "_G"
		local pp = path:sub(10) -- remove lua/auto/

		for s in pp:gmatch("([^/]+)") do
			if (s ~= "") then
				if (node[ s ]) then 
					node = node[ s ]
					namespace = s
				end
			end
		end
		
		local sname = fname:sub(1,-5)
		if (sname ~= namespace) then
			node = node[sname]
		end

		if (node and node.framework_run) then 
			node.framework_run()
		end
	end
	filesystem.fileForEach("lua/auto",true,{"lua"},iterator)
	
	-- Save configuration 
	core.config.save()
	
	-- Create window and context
	local wnd = window.create()
	
	CallbackSend("framework_preload")
	CallbackSend("framework_load")
	
	glfw.SetTime(0)
	local pt,frames,lastFrameTime = 0,0,0
	local function step()
		frames = frames + 1
		local tme = glfw.GetTime()
		if (tme - lastFrameTime >= 1) then
			_fps = frames
			frames = 0
			lastFrameTime = tme
		end
		
		local dt = tme - pt
		pt = tme
		return dt
	end

	-- Main application loop
	repeat
		glfw.PollEvents()
		
		local dt = step() -- Calculate FPS and delta-time
		local mouse_x,mouse_y = mouse.position()
		
		CallbackSend("framework_update",dt,mouse_x,mouse_y)

		local winW,winH = window.size()
		local fbW,fbH = window.framebufferSize()
		
		CallbackSend("framework_render",winW,winH,fbW,fbH)
		
		collectgarbage("step")
		--time.sleep(0.001)
	until (glfw.WindowShouldClose(wnd) == 1)
	
	CallbackSend("framework_exit")
end

function application_exit()
	collectgarbage()
	collectgarbage()
	
	window.destroy()
	glfw.Terminate()
end