function application_init()
	load()
end

function application_exit()
	save()
end

data = {}
function load()
	if not (filesystem.exists("user_config.lua")) then
		return
	end
	local chunk, errormsg = filesystem.loadfile("user_config.lua")
	if (chunk) then
		local env = {}
		setfenv(chunk,env) -- no access to _G
		
		local status,result = pcall(chunk)
		if (status) then
			print("imported user_config.lua")
			data = result
			return true
		end
		
		print("[ERROR] failed to load user_config.lua | " .. result)
	end
	print("[ERROR] " .. tostring(errormsg))
	return false
end

function save()
	filesystem.write("user_config.lua","return " .. print_table(data,true))
end

function r_value(table_index,key,def)
	return data[table_index] and data[table_index][key] or def
end

function w_value(table_index,key,value)
	if not (data[table_index]) then 
		data[table_index] = {}
	end
	data[table_index][key] = value
end

function set_default(table_index,key,value)
	if not (data[table_index]) then 
		data[table_index] = {}
	end
	if (data[table_index][key] == nil) then
		data[table_index][key] = value
	end
end