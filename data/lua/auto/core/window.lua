_window = nil

-- core.window()
getmetatable(this).__call = function(self)
	return _window
end

function framework_run()
	local section = "window"
	core.config.set_default(section,"title","")
	core.config.set_default(section,"width",1920)
	core.config.set_default(section,"height",1080)
	core.config.set_default(section,"fullscreen",false)
end

function create(config)
	glfw.SetErrorCallback(function(err,desc) print(ffi.string(desc)) end)

	if (glfw.Init() == 0) then
		error("glfw.Init() failed")
	end
	
	-- Configuration
	local config = core.config.data.window
	local x = config and tonumber(config.x) or 0
	local y = config and tonumber(config.y) or 0
	local width = config and tonumber(config.width) or 800
	local height = config and tonumber(config.height) or 600
	local fullscreen = config and config.fullscreen == true or false

	local monitor = glfw.GetPrimaryMonitor()
	local mode = glfw.GetVideoMode(monitor)

	glfw.WindowHint(glfw.CLIENT_API,glfw.NO_API)
	glfw.WindowHint(glfw.DOUBLEBUFFER,1)
	glfw.WindowHint(glfw.RED_BITS,mode.redBits)
	glfw.WindowHint(glfw.GREEN_BITS,mode.greenBits)
	glfw.WindowHint(glfw.BLUE_BITS,mode.blueBits)
	glfw.WindowHint(glfw.ALPHA_BITS,glfw.DONT_CARE)
	glfw.WindowHint(glfw.DEPTH_BITS,glfw.DONT_CARE)
	glfw.WindowHint(glfw.STENCIL_BITS,8)
	glfw.WindowHint(glfw.REFRESH_RATE,mode.refreshRate)
	
	if (ffi.os == "OSX") then
		glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR,3)
		glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR,2)
		glfw.WindowHint(glfw.OPENGL_FORWARD_COMPAT,1)
		glfw.WindowHint(glfw.OPENGL_PROFILE,glfw.OPENGL_CORE_PROFILE)
	end
	
	local wnd = nil
	if (fullscreen) then 
		wnd = glfw.CreateWindow(width,height,"",monitor,nil)
	else
		wnd = glfw.CreateWindow(width,height,"",nil,nil)
		glfw.SetWindowPos(wnd,(mode.width-width)/2,(mode.height-height)/2)
	end
	
	if (wnd == 0) then
		glfw.Terminate()
		error("Glfw window creation failed")
	end
	
	_window = wnd
	
	CallbackSend("framework_window_creation",wnd,width,height)
	
	glfw.PollEvents() -- crash fix for OSX

	-- Setup callbacks
	glfw.SetWindowSizeCallback(wnd,function(wnd,width,height)
		CallbackSend("framework_resize",width,height)
	end)
	local last_button_click = glfw.GetTime()
	glfw.SetMouseButtonCallback(wnd,function(wnd,button,action,mods)
		local x,y = core.mouse.position()
		if (action == glfw.PRESS) then
			local dt = glfw.GetTime() - last_button_click
			if (dt > 0.02 and dt < 0.2) then
				core.mouse.is_double_click_down = true
				core.mouse.double_click_pos[1] = x
				core.mouse.double_click_pos[2] = y
			end
			last_button_click = glfw.GetTime()
		else
			core.is_double_click_down = false
		end
		CallbackSend("framework_mousebutton",x,y,button,action,mods)
	end)
	glfw.SetWindowCloseCallback(wnd,function(wnd)
		CallbackSend("framework_window_close")
	end)
	local last_x,last_y = core.mouse.position()
	glfw.SetCursorPosCallback(wnd,function(wnd,x,y)
		local dx,dy = x-last_x,y-last_y
		last_x,last_y = x,y
		CallbackSend("framework_mousemoved",x,y,dx,dy)
	end)
	glfw.SetCursorEnterCallback(wnd,function(wnd,entered)
		CallbackSend("framework_mouseentered",entered == 1)
	end)
	glfw.SetKeyCallback(wnd,function(wnd,key,scancode,action,mods)
		-- https://www.glfw.org/docs/latest/group__keys.html
		local aliases = core.keyboard.keyToAlias[key]
		CallbackSend("framework_keyboard",key,aliases,scancode,action,mods)
	end)
	glfw.SetDropCallback(wnd,function(wnd,count,paths)
		for i=1,count do
			local path = paths[ i-1 ]
			if (path ~= nil) then
				local fname = ffi.string(path)
				CallbackSend("framework_filedropped",fname)
			end
		end
	end)
	glfw.SetScrollCallback(wnd,function(wnd,ox,oy)
		core.mouse.scroll_pos[ 1 ] = core.mouse.scroll_pos[ 1 ] + ox
		core.mouse.scroll_pos[ 2 ] = core.mouse.scroll_pos[ 2 ] + oy
		CallbackSend("framework_wheelmoved",core.mouse.scroll_pos[ 1 ],core.mouse.scroll_pos[ 2 ],ox,oy)
	end)
	glfw.SetCharModsCallback(wnd,function(wnd,codepoint,mods)
		CallbackSend("framework_textinput_mods",codepoint,mods)
	end)
	glfw.SetCharCallback(wnd,function(wnd,codepoint)
		CallbackSend("framework_textinput",codepoint)
	end)
	glfw.SetJoystickCallback(function(id,event)
		if (event == glfw.CONNECTED) then
			core.joystick.connect(id)
		elseif (event == glfw.DISCONNECTED) then
			core.joystick.disconnect(id)
		end
	end)
	glfw.SetWindowMaximizeCallback(wnd,function(wnd,maximized)
		-- maximized is 0 for restored and 1 for maximized
		CallbackSend("framework_maximized",maximized)
	end)
	
	return wnd
end

-- Get or Set window title
function title(str)
	if (str) then
		glfw.SetWindowTitle(_window,str)
		return str
	end
	return glfw.GetWindowTitle(_window)
end

-- Get or Set window width and height
function size(w,h)
	local _w,_h = ffi.new("int[1]"),ffi.new("int[1]")
	glfw.GetWindowSize(_window,_w,_h)
	if (w or h) then
		w = w or _w[0]
		h = h or _h[0]
		glfw.SetWindowSize(_window,w,h)
		return w,h
	end
	return _w[0],_h[0]
end

-- Get or Set window position
function position(x,y)
	local _x,_y = glfw.GetWindowPos(_window)
	if (x or y) then
		x = x or _x
		y = y or _y
		glfw.SetWindowPos(_window,x,y)
		return x,y
	end
	return _x,_y
end

-- Get framebuffer width and height
function framebufferSize()
    local width = ffi.new('int[1]')
    local height = ffi.new('int[1]')
    glfw.GetFramebufferSize(_window,width,height)
    return width[0], height[0]
end

function show()
	glfw.ShowWindow(_window)
	CallbackSend("framework_window_show",_window)
end

function hide()
	CallbackSend("framework_window_hide",_window)
	glfw.HideWindow(_window)
end

function destroy()
	if (_window ~= nil) then
		CallbackSend("framework_window_destroy",_window)
		glfw.DestroyWindow(_window)
		_window = nil
	end
end