local bgfx = require("ffi_bgfx") ; if not (bgfx) then error("Failed to load ffi_bgfx") end

local MAX_VERTEX_COUNT = 65536

-- core.import.image.batch()
getmetatable(this).__call = function (self)
	return cDynamicImageBatchGroup()
end

local function pushquad(data,index,x,y,w,h,ax,ay,tx,ty)
	local function pushv(x,y,u,v,rgba)
		local vert = ffi.cast("float *",data+index)
		vert[0] = x
		vert[1] = y
		vert[2] = u
		vert[3] = v
		vert = ffi.cast("uint32_t *",data+index+16)
		vert[0] = rgba
		index = index + 20
	end

	-- Counter-Clockwise winding order
	-- pushv(x,y,ax,ay,0xffffffff)		--  0_ 2			1 ___ 4
	-- pushv(x,y+h,ax,ty,0xffffffff)	--  | /		Verts:	 |	/|
	-- pushv(x+w,y,tx,ay,0xffffffff)	-- 1|/				2|/__|3
	
	-- pushv(x+w,y,tx,ay,0xffffffff)	--    3
	-- pushv(x,y+h,ax,ty,0xffffffff)	--   /|
	-- pushv(x+w,y+h,tx,ty,0xffffffff)	-- 4/_|5
	
	-- Clockwise winding order
	pushv(x,y,ax,ay,0xffffffff)		--  0_ 1
	pushv(x+w,y,tx,ay,0xffffffff)	--  | /
	pushv(x,y+h,ax,ty,0xffffffff)	-- 2|/
	
	pushv(x+w,y,tx,ay,0xffffffff)	--    3
	pushv(x+w,y+h,tx,ty,0xffffffff)	--   /|
	pushv(x,y+h,ax,ty,0xffffffff)	-- 5/_|4
end

------------------------------------------------------------------------
-- cDynamicImageBatchGroup
-- Simplifies the drawing of multiple image batches by grouping them by
-- texture idx
------------------------------------------------------------------------
Class "cDynamicImageBatchGroup"
function cDynamicImageBatchGroup:__init()
	self.batched = {}
end

--[[	exists
returns true if texture_info by specific alias exists in the batch group
--]]
function cDynamicImageBatchGroup:exists(texture_info)
	return self.batched[ texture_info.id ] ~= nil
end

--[[	add
pushes a quad using texture_info
--]]
function cDynamicImageBatchGroup:add(texture_info,ox,oy,sx,sy)
	if not (self.batched[ texture_info.id ]) then
		self.batched[ texture_info.id ] = cImageBatch()
	end
	
	self.batched[ texture_info.id ]:add(texture_info,ox,oy,sx,sy)
end

--[[	flush
Immediately sends sprite data to the graphics card.
--]]
function cDynamicImageBatchGroup:flush(view_id)
	for idx,image_batch in pairs(self.batched) do
		image_batch:flush(view_id)
	end
end

function cDynamicImageBatchGroup:isEmpty()
	for k,v in pairs(self.batched) do
		if not (v:isEmpty()) then
			return false
		end
	end
	return true
end

function cDynamicImageBatchGroup:isLoaded()
	for k,v in pairs(self.batched) do
		if not (v:isLoaded()) then
			return false
		end
	end
	return true
end

function cDynamicImageBatchGroup:clear()
	for k,v in pairs(self.batched) do
		self.batched[ k ] = nil
	end
end

------------------------------------------------------------------------
-- cImageBatch
-- Simplies drawing of many subimages that share the same base texture
-- as a single draw command
------------------------------------------------------------------------
Class "cImageBatch"
function cImageBatch:__init()
	self.vertices = ffi.new("unsigned char[?]",MAX_VERTEX_COUNT*20)
	self.vertex_count = 0
	self.index_by_alias = {}
	self.idx = 65535
	
	-- Generate the vertex declaration
	local vdecl = ffi.new('bgfx_vertex_layout_t[1]')
	bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_TEXCOORD0,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_COLOR0,4,bgfx.BGFX_ATTRIB_TYPE_UINT8,true,false)
	bgfx.bgfx_vertex_layout_end(vdecl)
	
	self.vdecl = vdecl
	self.vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)
end

--[[	add
pushes a quad using texture_info
--]]
function cImageBatch:add(texture_info,ox,oy,sx,sy)
	local idx = texture_info.id
	if (self.idx  ~= 65535 and self.idx ~= idx) then
		print("cImageBatch: Error you can only batch images that share the same base texture",texture_info.name)
	else
		self.idx = idx
	end

	local x,y,w,h = texture_info:getRegion()
	local width,height = texture_info:getTextureSize()
	
	local ax = texture_info.flipH and x / width + w / width or x / width
	local ay = texture_info.flipY and y / height + h / height or y / height
	local tx = texture_info.flipH and x / width or x / width + w / width
	local ty = texture_info.flipY and y / height or y / height + h / height

	sx = sx or 1
	sy = sy or 1
	pushquad(self.vertices,self.vertex_count,ox*sx,oy*sy,w*sx,h*sy,ax,ay,tx,ty)
	self.vertex_count = self.vertex_count + 120
end

--[[	flush
Immediately sends sprite data to the graphics card.
--]]
function cImageBatch:flush(view_id)
	local tex_h = ffi.new("bgfx_texture_handle_t",self.idx)
	local s_texColor = core.renderer.shader.uniform.s_texColor
	bgfx.bgfx_set_texture(0,s_texColor,tex_h,0xffffffff)

	bgfx.bgfx_set_state(bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)

	local tvb = ffi.new("bgfx_transient_vertex_buffer_t[1]")
	bgfx.bgfx_alloc_transient_vertex_buffer(tvb,self.vertex_count,self.vdecl)

	ffi.copy(tvb[ 0 ].data,self.vertices,self:size())
	
	bgfx.bgfx_set_transient_vertex_buffer_with_layout(0,tvb,0,self.vertex_count/20,self.vdecl_h)

	local u_scale = core.renderer.shader.uniform.u_scale
	local scale = ffi.new("float[4]",1,1,1,1)
	bgfx.bgfx_set_uniform(u_scale,scale,1)

	local prog = core.renderer.shader.programs.default
	bgfx.bgfx_submit(view_id,prog,0,bgfx.BGFX_DISCARD_ALL)
end

--[[	isLoaded
returns true if all textures in the group are loaded
--]]
function cImageBatch:isLoaded()
	return self.idx ~= 65535
end

--[[	isEmpty
returns true if there are no instances
--]]
function cImageBatch:isEmpty()
	return self.vertex_count == 0
end

--[[	clear
remove all textures from the buffer
--]]
function cImageBatch:clear()
	self.vertex_count = 0
end

--[[	size
returns buffer size
--]]
function cImageBatch:size()
	return self.vertex_count
end

--[[	quadCount
returns how many instances in the buffer
--]]
function cImageBatch:quadCount()
	return self.vertex_count / 120
end

--[[	vertexCount
returns how many vertices in the buffer
--]]
function cImageBatch:vertexCount()
	return self.vertex_count / 20
end

function cImageBatch:__gc()
	if (self.vdecl_h ~= nil) then
		bgfx.bgfx_destroy_vertex_layout(self.vdecl_h)
	end
end