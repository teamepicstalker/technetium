--[[
	Communication layer with renderer and asset manager to load and cache meshes.
--]]
local bgfx = require("ffi_bgfx") ; if not (bgfx) then error("Failed to load ffi_bgfx") end

cache = {}

-- core.import.model(filename,opts)
getmetatable(this).__call = function (self,fname,opts)
	if (cache[ fname ]) then
		return cache[ fname ]
	end
	local model = cModel(fname,opts)
	cache[ fname ] = model
	return model
end
------------------------------------------------------------------------
-- cModel
------------------------------------------------------------------------
Class "cModel"
function cModel:__init(filename,opts)
	self.name = filename
	self.meshes = {}
	self.bbox = {min={0,0,0},max={0,0,0}}
	
	local ext = utils.get_ext(filename)
	if (ext == "ogf") then
		self:load_ogf(filename,opts)
	elseif (ext == "gltf" or ext == "glb") then
		self:load_gltf(filename,opts)
	end
	
	--self:computeAABB()
end

-- https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html
function cModel:load_gltf(filename,opts)
	print(filename)
	local buffer = nil
	local function parse_file()
		local GlbHeaderSize = 12
		local GlbChunkHeaderSize = 8
		local GlbVersion = 2
		
		local node = core.import.binary(filename)
		assert(node ~= nil)
		
		-- validate header
		assert(node:size() > GlbHeaderSize)
		
		-- Is it binary or JSON?
		if not (node:r_string(4) == "glTF") then
			-- File is ASCII
			return json.decode(ffi.string(node.data,node:size()))
		end
		
		-- File is binary
		assert(node:r_u32() == GlbVersion)
		assert(node:r_u32() <= node:size())
		
		-- JSON chunk
		local json_length = node:r_u32()
		assert(json_length + GlbHeaderSize + GlbChunkHeaderSize <= node:size())
		assert(node:r_u32() == 0x4E4F534A) -- First chunk always contains JSON data, any other value is invalid
		
		local json_block = json_length > 0 and node:r_block(json_length)
		
		-- binary buffer chunk
		if (json_length + GlbHeaderSize + GlbChunkHeaderSize + GlbChunkHeaderSize < node:size()) then
			local length = node:r_u32()
			assert(length + json_length + GlbHeaderSize + GlbChunkHeaderSize <= node:size())
			assert(node:r_u32() == 0x004E4942) -- Chunk should contain ASCII 'BIN'
			
			buffer = core.import.binary.new(node:r_block(length),length)
		end
		
		if (json_block) then
			return json.decode(ffi.string(json_block,json_length))
		end
	end
	
	-- parse json to lua from either .gltf or .glb
	local json = parse_file()
	
	-- find buffer data if it hasn't been found already
	if (buffer == nil and json.buffers[0].uri ~= nil) then
		local a,b = string.find(json.buffers[0].uri,"data:application/octet%-stream;base64,")
		if (a) then
			local bin = ffi.new("const char*",utils.base64_decode(string.sub(json.buffers[0].uri,b+1)))
			buffer = core.import.binary.new(bin,json.buffers[0].byteLength)
		elseif (utils.get_ext(json.buffers[0].uri) == "bin") then
			buffer = core.import.binary(utils.get_path(filename) .. json.buffers[0].uri)
		end
	end
	
	assert(buffer ~= nil)
	
	local types = {
		SCALAR = 1,
		VEC2 = 2,
		VEC3 = 3,
		VEC4 = 4,
		MAT2 = 4,
		MAT3 = 9,
		MAT4 = 16
	}

	local attribute_def = {
		["POSITION"] = "float POSITION[3];\n",
		["NORMAL"] = "float NORMAL[3];\n",
		["TANGENT"] = "float TANGENT[4];\nfloat BITANGENT[3];\n",
		["COLOR_0"] = "float COLOR_0[4];\n",
		["COLOR_1"] = "float COLOR_1[4];\n",
		["COLOR_2"] = "float COLOR_2[4];\n",
		["COLOR_3"] = "float COLOR_3[4];\n",
		["TEXCOORD_0"] = "float TEXCOORD_0[2];\n",
		["TEXCOORD_1"] = "float TEXCOORD_1[2];\n",
		["TEXCOORD_2"] = "float TEXCOORD_2[2];\n",
		["TEXCOORD_3"] = "float TEXCOORD_3[2];\n",
		["TEXCOORD_4"] = "float TEXCOORD_4[2];\n",
		["TEXCOORD_5"] = "float TEXCOORD_5[2];\n",
		["TEXCOORD_6"] = "float TEXCOORD_6[2];\n",
		["TEXCOORD_7"] = "float TEXCOORD_7[2];\n"
	}
		
	-- we only care and support a single mesh
	for _,primitive in pairs(json.meshes[0].primitives) do -- "pairs" because our json decode starts tables at index 0. "ipairs" starts at 1.
		assert(primitive.attributes.POSITION ~= nil)
		assert(primitive.indices ~= nil)
		
		-- Begin our BGFX vertex layout
		local vdecl = ffi.new('bgfx_vertex_layout_t[1]')
		bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
		
		-- construct a C Definition for our vertex declaration
		local layout = { ptr = nil, stride = 0 } -- ctype layout to cast our data into
		local cdef_name = ""
		local cdef = [[
		struct { 
		]]
		
		cdef = cdef .. attribute_def.POSITION
		layout.stride = layout.stride + 3*ffi.sizeof("float")
		if (primitive.attributes.NORMAL ~= nil) then
			cdef = cdef .. attribute_def.NORMAL
			layout.stride = layout.stride + 3*ffi.sizeof("float")
		end
		if (primitive.attributes.TANGENT ~= nil) then
			cdef = cdef .. attribute_def.TANGENT
			layout.stride = layout.stride + 7*ffi.sizeof("float")
		end
		if (primitive.attributes.COLOR_0 ~= nil) then
			cdef = cdef .. attribute_def.COLOR_0
			layout.stride = layout.stride + 4*ffi.sizeof("float")
		end		
		if (primitive.attributes.COLOR_1 ~= nil) then
			cdef = cdef .. attribute_def.COLOR_1
			layout.stride = layout.stride + 4*ffi.sizeof("float")
		end
		if (primitive.attributes.COLOR_2 ~= nil) then
			cdef = cdef .. attribute_def.COLOR_2
			layout.stride = layout.stride + 4*ffi.sizeof("float")
		end
		if (primitive.attributes.COLOR_3 ~= nil) then
			cdef = cdef .. attribute_def.COLOR_3
			layout.stride = layout.stride + 4*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_0 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_0
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end			
		if (primitive.attributes.TEXCOORD_1 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_1
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_2 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_2
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_3 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_3
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_4 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_4
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end		
		if (primitive.attributes.TEXCOORD_5 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_5
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_6 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_6
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		if (primitive.attributes.TEXCOORD_7 ~= nil) then
			cdef = cdef .. attribute_def.TEXCOORD_7
			layout.stride = layout.stride + 2*ffi.sizeof("float")
		end
		
		cdef = cdef .. "	} *"
		
		-- create a new struct for the vertex layout
		layout.ptr = ffi.typeof(cdef)

		-- indices
		local indices = nil
		
		local accessor = json.accessors[primitive.indices]
		buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
		buffer:r_advance(accessor.byteOffset or 0)
		
		if (accessor.componentType == 5123) then
			indices = ffi.new("uint16_t ["..accessor.count.."]")
			for j=0,accessor.count-1 do
				indices[j] = buffer:r_u16()
			end
		elseif (accessor.componentType == 5125) then
			indices = ffi.new("uint32_t ["..accessor.count.."]")
			for j=0,accessor.count-1 do
				indices[j] = buffer:r_u32()
			end
		else
			error("model:open_gltf() := unsupported type for indices, try uint16 or uint32")
		end
		
		-- TODO: reverse winding-order option?
		-- for j=0,accessor.count,3 do
			-- local tmp = indices[j]
			-- indices[j] = indices[j+2]
			-- indices[j+2] = tmp
		-- end
		
		-- create the index buffer
		local index_buffer_object = bgfx.bgfx_create_index_buffer(bgfx.bgfx_make_ref(indices,ffi.sizeof(indices)),accessor.componentType == 5125 and bgfx.BGFX_BUFFER_INDEX32 or bgfx.BGFX_BUFFER_NONE)
		
		-- Build interleaved vertices

		-- POSITION
		local name = "POSITION"
		
		bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,3,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
		
		accessor = json.accessors[ primitive.attributes[ name ] ]

		local vertices_data = ffi.new("unsigned char[?]",layout.stride*accessor.count)
		local vertices = ffi.cast(layout.ptr,vertices_data)

		assert(accessor.componentType == 5126) -- should be float unless malformed glTF
		local cnt = accessor.count
		local stride = json.bufferViews[ accessor.bufferView ].byteStride or 0
		buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
		buffer:r_advance(accessor.byteOffset or 0)
		for j=0,cnt-1 do
			local pos = buffer:r_tell()
			vertices[j][name][0] = buffer:r_float()
			vertices[j][name][1] = buffer:r_float()
			vertices[j][name][2] = buffer:r_float()
			if (stride > 0) then
				buffer:r_advance(stride - (buffer:r_tell()-pos))
			end
		end

		-- NORMAL
		name = "NORMAL"
		if (primitive.attributes[ name ]) then
			bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_NORMAL,3,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
			
			local accessor = json.accessors[ primitive.attributes[ name ] ]
			assert(accessor.componentType == 5126) -- should be float unless malformed glTF
			local cnt = accessor.count
			local stride = json.bufferViews[ accessor.bufferView ].byteStride or 0
			buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
			buffer:r_advance(accessor.byteOffset or 0)
			for j=0,cnt-1 do
				local pos = buffer:r_tell()
				vertices[j][name][0] = buffer:r_float()
				vertices[j][name][1] = buffer:r_float()
				vertices[j][name][2] = buffer:r_float()
				if (stride > 0) then
					buffer:r_advance(stride - (buffer:r_tell()-pos))
				end
			end
		end
		
		-- TANGENT
		name = "TANGENT"
		if (primitive.attributes[ name ]) then
			bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_TANGENT,4,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
			bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_BITANGENT,3,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
			
			local accessor = json.accessors[ primitive.attributes[ name ] ]
			assert(accessor.componentType == 5126) -- should be float unless malformed glTF
			local cnt = accessor.count
			local stride = json.bufferViews[ accessor.bufferView ].byteStride or 0
			buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
			buffer:r_advance(accessor.byteOffset or 0)
			for j=0,cnt-1 do
				local pos = buffer:r_tell()
				vertices[j][name][0] = buffer:r_float()
				vertices[j][name][1] = buffer:r_float()
				vertices[j][name][2] = buffer:r_float()
				vertices[j][name][3] = buffer:r_float()
				
				-- BITANGENT
				local normal = ffi.new("vec3_t",vertices[j]["NORMAL"][0],vertices[j]["NORMAL"][1],vertices[j]["NORMAL"][2])
				local tangent = ffi.new("vec3_t",vertices[j]["TANGENT"][0],vertices[j]["TANGENT"][1],vertices[j]["TANGENT"][2])
				local bitangent = normal:cross(tangent:unpack())
				local handedness = core.renderer.is_right_handed() and 1 or -1
				vertices[j]["BITANGENT"][0] = handedness * bitangent.x
				vertices[j]["BITANGENT"][1] = handedness * bitangent.y
				vertices[j]["BITANGENT"][2] = handedness * bitangent.z
				if (stride > 0) then
					buffer:r_advance(stride - (buffer:r_tell()-pos))
				end
			end
		end
		
		-- COLOR_n (Here we need to deviate from GlTF specification because bgfx doesn't have uint16 attribute type; instead we'll normalize to float)
		for n=0,3 do
			name = "COLOR_"..n
			if (primitive.attributes[ name ]) then
				bgfx.bgfx_vertex_layout_add(vdecl,bgfx["BGFX_ATTRIB_COLOR"..n],4,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)

				local accessor = json.accessors[ primitive.attributes[ name ] ]
				local cnt = accessor.count
				local stride = json.bufferViews[ accessor.bufferView ].byteStride or 0
				buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
				buffer:r_advance(accessor.byteOffset or 0)
				if (accessor.componentType == 5121) then
					assert(accessor.normalized == true)
					local max_value = ffi.new("uint8_t [1]",-1)
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_u8() / max_value[0]
						vertices[j][name][1] = buffer:r_u8() / max_value[0]
						vertices[j][name][2] = buffer:r_u8() / max_value[0]
						vertices[j][name][3] = accessor.type == "VEC3" and 0 or buffer:r_u8() / max_value[0]
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end						
				elseif (accessor.componentType == 5123) then
					assert(accessor.normalized == true)
					local max_value = ffi.new("uint16_t [1]",-1)
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_u16() / max_value[0]
						vertices[j][name][1] = buffer:r_u16() / max_value[0]
						vertices[j][name][2] = buffer:r_u16() / max_value[0]
						vertices[j][name][3] = accessor.type == "VEC3" and 0 or buffer:r_u16() / max_value[0]
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end
				elseif (accessor.componentType == 5126) then
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_float()
						vertices[j][name][1] = buffer:r_float()
						vertices[j][name][2] = buffer:r_float()
						vertices[j][name][3] = accessor.type == "VEC3" and 0 or buffer:r_float()
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end
				else
					error("malformed glTF, unsupported type for COLOR")					
				end
			end
		end
		
		-- TEXCOORD_n
		for n=0,7 do
			name = "TEXCOORD_"..n
			if (primitive.attributes[ name ]) then
				bgfx.bgfx_vertex_layout_add(vdecl,bgfx["BGFX_ATTRIB_TEXCOORD"..n],2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
				
				local accessor = json.accessors[ primitive.attributes[ name ] ]
				local cnt = accessor.count
				local stride = json.bufferViews[ accessor.bufferView ].byteStride or 0
				buffer:r_seek(json.bufferViews[ accessor.bufferView ].byteOffset or 0)
				buffer:r_advance(accessor.byteOffset or 0)
				if (accessor.componentType == 5121) then
					assert(accessor.normalized == true)
					local max_value = ffi.new("uint8_t [1]",-1)
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_u8() / max_value[0]
						vertices[j][name][1] = buffer:r_u8() / max_value[0]
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end						
				elseif (accessor.componentType == 5123) then
					assert(accessor.normalized == true)
					local max_value = ffi.new("uint16_t [1]",-1)
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_u16() / max_value[0]
						vertices[j][name][1] = buffer:r_u16() / max_value[0]
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end
				elseif (accessor.componentType == 5126) then
					for j=0,cnt-1 do
						local pos = buffer:r_tell()
						vertices[j][name][0] = buffer:r_float()
						vertices[j][name][1] = buffer:r_float()
						if (stride > 0) then
							buffer:r_advance(stride - (buffer:r_tell()-pos))
						end
					end
				else
					error("malformed glTF, unsupported type for TEXCOORD")
				end
			end
		end
		
		bgfx.bgfx_vertex_layout_end(vdecl)
		local vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)
		
		-- create the vertex buffer
		local vertex_buffer_object = bgfx.bgfx_create_vertex_buffer(bgfx.bgfx_make_ref(vertices_data,ffi.sizeof(vertices_data)),vdecl,bgfx.BGFX_BUFFER_NONE)
		
		-- instance the sub mesh
		local oMesh = cMesh()
		oMesh.index_buffer_object = index_buffer_object
		oMesh.vertex_buffer_object = vertex_buffer_object
		oMesh.vertices = vertices_data
		oMesh.vertex_layout = layout
		oMesh.indices = indices
		oMesh.vdecl = vdecl
		oMesh.vdecl_h = vdecl_h
		
		table.insert(self.meshes, oMesh)
	end
end

function cModel:draw(view_id)
	for i, mesh in ipairs(self.meshes) do
		mesh:draw(view_id)
	end
end

function cModel:drawAABB()

end

function cModel:computeAABB()
	for i,o in ipairs(self.meshes) do
		o:computeAABB()
		self.bbox.min[1] = math.min(self.bbox.min[1],o.bbox.min[1])
		self.bbox.min[2] = math.min(self.bbox.min[2],o.bbox.min[2])
		self.bbox.min[3] = math.min(self.bbox.min[3],o.bbox.min[3])

		self.bbox.max[1] = math.max(self.bbox.max[1],o.bbox.max[1])
		self.bbox.max[2] = math.max(self.bbox.max[2],o.bbox.max[2])
		self.bbox.max[3] = math.max(self.bbox.max[3],o.bbox.max[3])
	end
end

function cModel:__gc()

end
------------------------------------------------------------------------
-- cMesh
------------------------------------------------------------------------
Class "cMesh"
function cMesh:__init()
	self.bbox = {min={0,0,0},max={0,0,0}}
	self.textures = {}
	self.material = nil
end

function cMesh:draw(view_id)
	local view_id = view_id or 0
	
	bgfx.bgfx_set_state(bgfx.BGFX_STATE_WRITE_MASK+bgfx.BGFX_STATE_DEPTH_TEST_LESS+bgfx.BGFX_STATE_MSAA,0)
	
	bgfx.bgfx_set_vertex_buffer(0,self.vertex_buffer_object,0,ffi.sizeof(self.vertices))
	bgfx.bgfx_set_index_buffer(self.index_buffer_object,0,ffi.sizeof(self.indices))
	
	local prog = core.renderer.shader.programs.cubes
	bgfx.bgfx_submit(view_id,prog,0,false)	
end

function cMesh:drawAABB()

end

-- TODO: Can technically be done from glTF importing using min/max
function cMesh:computeAABB()
	local stride = self.vertex_layout.stride
	local cnt = ffi.sizeof(self.vertices)/stride
	local vertices = ffi.cast(self.vertex_layout.ptr,self.vertices)
	for i=0,cnt-1 do
		local x = vertices[i].POSITION[0]
		local y = vertices[i].POSITION[1]
		local z = vertices[i].POSITION[2]
		
		if (i == 0) then
			self.bbox.min[1],self.bbox.max[1] = x,x
			self.bbox.min[2],self.bbox.max[2] = y,y
			self.bbox.min[3],self.bbox.max[3] = z,z
		else
			self.bbox.min[1] = math.min(self.bbox.min[1], x)
			self.bbox.min[2] = math.min(self.bbox.min[2], y)
			self.bbox.min[3] = math.min(self.bbox.min[3], z)

			self.bbox.max[1] = math.max(self.bbox.max[1], x)
			self.bbox.max[2] = math.max(self.bbox.max[2], y)
			self.bbox.max[3] = math.max(self.bbox.max[3], z)
		end
	end
end

function cMesh:__gc()
	if (self.vdecl_h ~= nil) then
		bgfx.bgfx_destroy_vertex_layout(self.vdecl_h)
	end
end