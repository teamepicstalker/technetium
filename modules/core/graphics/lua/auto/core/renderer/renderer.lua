local bgfx = require("ffi_bgfx") ; if not (bgfx) then error("Failed to load ffi_bgfx") end
local stbi = require("ffi_stb_image")

bgfx_init_s = ffi.new("bgfx_init_t[1]")
_view_rect = {}

-- TODO: Add support for .ktx
-- see: https://github.com/stetre/moonsoil/blob/master/moonsoil/ktx.lua
--[[
// HEADER
0xAB, 0x4B, 0x54, 0x58, // first four bytes of Byte[12] identifier
0x20, 0x31, 0x31, 0xBB, // next four bytes of Byte[12] identifier
0x0D, 0x0A, 0x1A, 0x0A, // final four bytes of Byte[12] identifier
0x04, 0x03, 0x02, 0x01, // Byte[4] endianess (Big endian in this case)
0x00, 0x00, 0x00, 0x00, // UInt32 glType = 0
0x00, 0x00, 0x00, 0x01, // UInt32 glTypeSize = 1
0x00, 0x00, 0x00, 0x00, // UInt32 glFormat = 0
0x00, 0x00, 0x8D, 0x64, // UInt32 glInternalFormat = GL_ETC1_RGB8_OES
0x00, 0x00, 0x19, 0x07, // UInt32 glBaseInternalFormat = GL_RGB
0x00, 0x00, 0x00, 0x20, // UInt32 pixelWidth = 32
0x00, 0x00, 0x00, 0x20, // UInt32 pixelHeight = 32
0x00, 0x00, 0x00, 0x00, // UInt32 pixelDepth = 0
0x00, 0x00, 0x00, 0x00, // UInt32 numberOfArrayElements = 0
0x00, 0x00, 0x00, 0x01, // UInt32 numberOfFaces = 1
0x00, 0x00, 0x00, 0x01, // UInt32 numberOfMipmapLevels = 1
0x00, 0x00, 0x00, 0x10, // UInt32 bytesOfKeyValueData = 16
// METADATA
0x00, 0x00, 0x00, 0x0A, // UInt32 keyAndValueByteSize = 10
0x61, 0x70, 0x69, 0x00, // UTF8 key:   'api\0'
0x67, 0x6C, 0x65, 0x73, // UTF8 v: 'gles2\0'
0x32, 0x00, 0x00, 0x00, // Byte[2] valuePadding (2 bytes)
// TEXTURE DATA
0x00, 0x00, 0x02, 0x00, // UInt32 imageSize = 512 bytes
0xD8, 0xD8, 0xD8, 0xDA, // Byte[512] ETC compressed texture data...
...
--]]

------------------------------------------------
-- API
------------------------------------------------
function get_video_options()
	local config = core.config.data.renderer
	local vsync = config and config.vsync == true or false
	local msaa = config and tonumber(config.msaa) or 0
	if not (msaa == 2 or msaa == 4 or msaa == 8 or msaa == 16) then
		msaa = 0
	end
	local video_flags = vsync and bgfx.BGFX_RESET_VSYNC or bgfx.BGFX_RESET_NONE
	if (msaa > 0) then
		video_flags = video_flags + bgfx[ "BGFX_RESET_MSAA_X"..msaa ]
	end
	--video_flags = video_flags + bgfx.BGFX_RESET_MAXANISOTROPY
	return video_flags
end

function is_right_handed()
	return false
end

function is_homogeneous_depth()
	return bgfx.bgfx_get_caps().homogeneousDepth
end

--[[	set_transform
@ param1: modelMatrix
@ param2: index
--]]
function set_transform(modelMatrix,index)
	bgfx.bgfx_set_transform(modelMatrix,index)
end

--[[	set_view_transform
@ param1: view_id
@ param2: viewMatrix
@ param3: projectionMatrix
--]]
function set_view_transform(view_id,viewMatrix,projectionMatrix)
	bgfx.bgfx_set_view_transform(view_id or 0,viewMatrix,projectionMatrix)
end

--[[	create_texture
@ param1: pixel data
@ param2: table containing texture parameters
--]]
function create_texture(pixels,texture_info)
	texture_info.format = texture_info.format or bgfx.BGFX_TEXTURE_FORMAT_RGBA8
	local w = texture_info.width
	local h = texture_info.height
	local caps = bgfx.bgfx_get_caps()
	local layers = bit.band(caps.formats[ 0 ],bgfx.BGFX_CAPS_TEXTURE_2D_ARRAY) > 0 and texture_info.numLayers or 1
	
	local bgfx_owned_mem = bgfx.bgfx_copy(pixels,w*h*4)
	local tex_h = bgfx.bgfx_create_texture_2d(w,h,texture_info.numMips > 1,layers,texture_info.format,texture_info.flags,bgfx_owned_mem)
	texture_info.id = tex_h.idx
	
	texture_info.loaded = true
	
	return texture_info
end

--[[	open_texture
@ param1: vfs file path
@ param2: table that will be filled with texture parameters
--]]
function open_texture(filename)
	if not (filesystem.exists(filename)) then
		print("doesn't exist",filename)
		return
	end
	
	local buffer,length = filesystem.read(filename)
	local w,h,comp = ffi.new("int[1]"),ffi.new("int[1]"),ffi.new("int[1]")

	local pixels = stbi.load_from_memory(buffer,length,w,h,comp,4)
	
	local texture_info = core.import.image.cache[ filename ]
	texture_info.name = filename
	texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8
	texture_info.width = w[0]
	texture_info.height = h[0]
	texture_info.numLayers = 1
	texture_info.numMips =  1
	texture_info.flags = 0

	-- uv region
	texture_info.x = 0
	texture_info.y = 0
	texture_info.w = texture_info.width
	texture_info.h = texture_info.height	
	
	local bgfx_owned_mem = bgfx.bgfx_copy(pixels,w[0]*h[0]*comp[0])
	stbi.image_free(pixels)
	
	local tex_h = bgfx.bgfx_create_texture_2d(w[0],h[0],texture_info.numMips > 1,texture_info.numLayers,texture_info.format,texture_info.flags,bgfx_owned_mem)
	
	texture_info.id = tex_h.idx
	texture_info.loaded = true
	return texture_info
end

--[[	open_texture_threaded
@ param1: vfs file path
Sends an operation to the thread pool to open a texture and fill a table with texture information and copy it to main thread's cached texture info data
--]]
function open_texture_threaded(filename)
	if not (filesystem.exists(filename)) then
		return
	end
	if not (core.thread.pool.enabled) then
		open_texture(filename)
		return
	end
	
	local imageList = {}
	core.thread.pool.linda:send("pool-functor",{function(a,b,c)
		local bgfx = require("ffi_bgfx")
		local stbi = require("ffi_stb_image")

		local buffer,length = filesystem.read(filename)
		local w,h,comp = ffi.new("int[1]"),ffi.new("int[1]"),ffi.new("int[1]")

		local pixels = stbi.load_from_memory(buffer,length,w,h,comp,4)

		local new_texture_info = {}
		new_texture_info.name = filename
		new_texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8
		new_texture_info.width = w[0]
		new_texture_info.height = h[0]
		new_texture_info.numLayers = 1
		new_texture_info.numMips =  1
		new_texture_info.flags = 0

		-- uv region
		new_texture_info.x = 0
		new_texture_info.y = 0
		new_texture_info.w = new_texture_info.width
		new_texture_info.h = new_texture_info.height	
		
		local bgfx_owned_mem = bgfx.bgfx_copy(pixels,w[0]*h[0]*comp[0])
		stbi.image_free(pixels)
		
		local tex_h = bgfx.bgfx_create_texture_2d(w[0],h[0],new_texture_info.numMips > 1,new_texture_info.numLayers,new_texture_info.format,new_texture_info.flags,bgfx_owned_mem)
		
		new_texture_info.id = tex_h.idx
		
		linda:send("main-functor",{function()
			local cache = core.import.image.cache
			local texture_info = cache[ new_texture_info.name ]
			texture_info:copy(new_texture_info)
			texture_info.loaded = true
		end})
	end})
end

--[[	blit_texture
@ param1: table containing new texture info
@ param2: table containing texture info of a source texture
@ param3: x offset of source image
@ param4: y offset of source image
@ param5: w offset of source image
@ param6: h offset of source image
--]]
function blit_texture(texture_info,src_texture_info,x,y,w,h)
	local bgfx_owned_mem = bgfx.bgfx_alloc(w*h*4)
	local tex_h = bgfx.bgfx_create_texture_2d(w,h,false,1,bgfx.BGFX_TEXTURE_FORMAT_RGBA8,bgfx.BGFX_TEXTURE_BLIT_DST,bgfx_owned_mem)
	local src_tex_h = ffi.new("bgfx_texture_handle_t",src_texture_info.id)
	bgfx.bgfx_blit(0,tex_h,0,0,0,0,src_tex_h,0,x,y,0,w,h,1)
	
	texture_info.id = tex_h.idx

	texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8
	texture_info.storageSize = w*h*4
	texture_info.width = w
	texture_info.height = h
	texture_info.depth = 1
	texture_info.numLayers = 1
	texture_info.numMips =  1
	texture_info.bitsPerPixel = 32
	texture_info.cubeMap = false
	texture_info.flags = 0 -- bgfx.BGFX_TEXTURE_NONE
	
	-- uv region
	texture_info.x = 0
	texture_info.y = 0
	texture_info.w = w
	texture_info.h = h
end

--[[	destroy_texture
@ param1: table containing texture parameters
--]]
function destroy_texture(texture_info)
	texture_info.loaded = false
	if (texture_info.id and texture_info.id ~= 65535) then
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		bgfx.bgfx_destroy_texture(tex_h)
		texture_info.id = 65535
	end
end

--[[	draw_texture
@ param1: table containing texture parameters
@ param2: optional encoder created with 'bgfx_begin_encoder'
Quick and dirty! Mainly use for debug/prototyping.
Create your own optimized draw method suited
to your design or use core.import.image.batch.
--]]
function draw_texture(texture_info,view_id,encoder)
	if not (texture_info.loaded) then
		return
	end
	
	local vdecl = ffi.new('bgfx_vertex_layout_t[1]')
	bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_TEXCOORD0,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_COLOR0,4,bgfx.BGFX_ATTRIB_TYPE_UINT8,true,false)
	bgfx.bgfx_vertex_layout_end(vdecl)
	local vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)
	
	view_id = view_id or 0
	
	local ax = texture_info.x / texture_info.width
	local ay = texture_info.y / texture_info.height
	local tx = ax + texture_info.w / texture_info.width
	local ty = ay + texture_info.h / texture_info.height

	local tvb = ffi.new('bgfx_transient_vertex_buffer_t[1]')
	bgfx.bgfx_alloc_transient_vertex_buffer(tvb,6,vdecl)
	
	local cnt = 0
	local function pushv(x,y,u,v,rgba)
		local vert = ffi.cast("float *",tvb[ 0 ].data+cnt)
		vert[0] = x
		vert[1] = y
		vert[2] = u
		vert[3] = v
		vert = ffi.cast("uint32_t *",tvb[ 0 ].data+cnt+16)
		vert[0] = rgba
		cnt = cnt + 20
	end
	
	local function pushquad(x, y, w, h)
		pushv(x,y,ax,ay,0xffffffff)
		pushv(x,y+h,ax,ty,0xffffffff)
		pushv(x+w,y,tx,ay,0xffffffff)
		pushv(x+w,y,tx,ay,0xffffffff)
		pushv(x,y+h,ax,ty,0xffffffff)
		pushv(x+w,y+h,tx,ty,0xffffffff)
	end

	pushquad(0,0,texture_info.w,texture_info.h)
	
	local flipH, flipV = texture_info.flipH, texture_info.flipV
	if (encoder) then
		bgfx.bgfx_encoder_set_state(encoder,bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)
		bgfx.bgfx_encoder_set_transient_vertex_buffer(encoder,0,tvb,0,6,vdecl_h)
		
		local u_scale = core.renderer.shader.uniform.u_scale
		local scale = ffi.new("float[4]",flipH and -1 or 1,flipV and -1 or 1,1,1)
		bgfx.bgfx_encoder_set_uniform(encoder,u_scale,scale,1)
		
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		local s_texColor = core.renderer.shader.uniform.s_texColor
		bgfx.bgfx_encoder_set_texture(encoder,0,s_texColor,tex_h,0xffffffff)
		
		local prog = core.renderer.shader.programs.default
		bgfx.bgfx_encoder_submit(encoder,view_id,prog,0,bgfx.BGFX_DISCARD_ALL)
	else
		bgfx.bgfx_set_state(bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)
		bgfx.bgfx_set_transient_vertex_buffer(0,tvb,0,6)
		
		local u_scale = core.renderer.shader.uniform.u_scale
		local scale = ffi.new("float[4]",flipH and -1 or 1,flipV and -1 or 1,1,1)
		bgfx.bgfx_set_uniform(u_scale,scale,1)
		
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		local s_texColor = core.renderer.shader.uniform.s_texColor
		bgfx.bgfx_set_texture(0,s_texColor,tex_h,0xffffffff)
		
		local prog = core.renderer.shader.programs.default
		bgfx.bgfx_submit(view_id,prog,0,bgfx.BGFX_DISCARD_ALL)	
	end
	
	bgfx.bgfx_destroy_vertex_layout(vdecl_h)
end

--[[	create_framebuffer_from_texture
@ param1: texture_info
Uses an existing texture as a framebuffer (ie. render to texture)
--]]
function create_framebuffer_from_texture(texture_info)
	--  void bgfx_attachment_init(bgfx_attachment_t* _this, bgfx_texture_handle_t _handle, bgfx_access_t _access, uint16_t _layer, uint16_t _numLayers, uint16_t _mip, uint8_t _resolve);
	local attachment = ffi.new("bgfx_attachment_t[1]")
	local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
	bgfx.bgfx_attachment_init(attachment,tex_h,bgfx.BGFX_ACCESS_READWRITE,1,texture_info.numLayers,texture_info.numMips,bgfx.BGFX_RESOLVE_AUTO_GEN_MIPS)
	local framebuffer = bgfx.bgfx_create_frame_buffer_from_attachment(1,attachment,false)
	ffi.gc(framebuffer,function(fbuf) return bgfx.bgfx_destroy_frame_buffer(fbuf) end)
	return framebuffer
end

--[[	reset
@ param1: resoltuion width
@ param2: resolution height
reset the renderer to desired resolution
--]]
function reset(width,height)
	local video_flags = get_video_options()
	bgfx.bgfx_reset(width,height,video_flags,bgfx_init_s[0].resolution.format)
end

--[[	set_view_rect
@ param1: view id
@ param2: x offset
@ param3: y offset
@ param4: width
@ param5: height
set view_rect to given parameters
--]]
function set_view_rect(view_id,x,y,w,h)
	if not (_view_rect[ view_id ]) then
		_view_rect[ view_id ] = {}
	end
	
	local view = _view_rect[ view_id ]
	
	view.x = x
	view.y = y
	view.w = w
	view.h = h
	
	bgfx.bgfx_set_view_rect(view_id,x,y,w,h)
end

--[[	get_view_rect
@ param1: view id
returns view_rect x,y,w,h
--]]
function get_view_rect(view_id)
	local view = _view_rect[ view_id ]
	return view.x,view.y,view.w,view.h
end

------------------------------------------------
-- Callbacks 
------------------------------------------------
local function on_framework_render(winW,winH,fbW,fbH)
	bgfx.bgfx_touch(0)
	--bgfx.bgfx_dbg_text_printf(0,0,0x2f,"FPS: "..tostring(core.getFPS()))
end

local function on_framework_frame_end(winW,winH,fbW,fbH)
	bgfx.bgfx_frame(false)
end

local function on_framework_window_creation(wnd,width,height)
	bgfx.bgfx_init_ctor(bgfx_init_s)
	bgfx_init_s[0].type = bgfx.BGFX_RENDERER_TYPE_COUNT
	bgfx_init_s[0].vendorId = bgfx.BGFX_PCI_ID_NONE
	bgfx_init_s[0].deviceId = 0
	bgfx_init_s[0].debug = false
	bgfx_init_s[0].profile = false
	bgfx_init_s[0].resolution.width = width
	bgfx_init_s[0].resolution.height = height
	bgfx_init_s[0].resolution.reset = bgfx.BGFX_RESET_VSYNC
	bgfx_init_s[0].resolution.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8
	
	-- Retrieve platform data
	if (ffi.os == "Windows") then
		local nwh = ffi.cast("void*",glfw.GetWin32Window(wnd))
		bgfx_init_s[0].platformData.nwh = nwh
	elseif (ffi.os == "OSX") then
		local nwh = ffi.cast("void*",glfw.GetCocoaWindow(wnd))
		bgfx_init_s[0].platformData.nwh = nwh
	elseif (ffi.os == "Linux") then
		-- TODO: Determine if these libs are necessary
		ffi.load('/usr/lib/libX11.so.6', true)
		ffi.load('/usr/lib/libGL.so', true)
		
		local nwh = ffi.cast("void *",glfw.GetX11Window(wnd))
		local ndt = ffi.cast("void *",glfw.GetX11Display())
		bgfx_init_s[0].platformData.nwh = nwh
		bgfx_init_s[0].platformData.ndt = ndt
	else
		error("Unsupported platform: " .. ffi.os)
	end
	
	-- Initialize bgfx
	bgfx.bgfx_init(bgfx_init_s[0])
	
	bgfx.bgfx_set_debug(bgfx.BGFX_DEBUG_TEXT)
	
	local renderer = bgfx.bgfx_get_renderer_name(bgfx.bgfx_get_renderer_type())
	print("renderer:",renderer ~= nil and ffi.string(renderer) or "unknown")

	local video_flags = get_video_options()
	bgfx.bgfx_reset(width,height,video_flags,bgfx_init_s[0].resolution.format)
	set_view_rect(0,0,0,width,height)
	bgfx.bgfx_set_view_clear(0,bgfx.BGFX_CLEAR_COLOR + bgfx.BGFX_CLEAR_DEPTH,0x336666ff,1.0,0)
end

local function on_framework_resize(w,h)
	bgfx_init_s[0].resolution.width = w
	bgfx_init_s[0].resolution.height = h
	
	-- TODO: 
	--bgfx.bgfx_reset(w,h,get_video_options(),bgfx_init_s[0].resolution.format)
	--bgfx.bgfx_set_view_rect(0,0,0,w,h)
	--bgfx.bgfx_set_view_clear(0,bgfx.BGFX_CLEAR_COLOR + bgfx.BGFX_CLEAR_DEPTH,0x999966,1.0,0)
end

local function on_framework_window_destroy(wnd)
	bgfx.bgfx_shutdown()
end

function framework_run()
	local section = "renderer"
	core.config.set_default(section,"renderer_type",bgfx.BGFX_RENDERER_TYPE_OPENGL)
	core.config.set_default(section,"msaa",0)
	core.config.set_default(section,"vsync",false)
	
	CallbackSet("framework_window_creation",on_framework_window_creation,9999)
	CallbackSet("framework_window_destroy",on_framework_window_destroy,9999)
	CallbackSet("framework_resize",on_framework_resize,9999)
	CallbackSet("framework_render",on_framework_render,9999)
	CallbackSet("framework_render",on_framework_frame_end,-9999)
end