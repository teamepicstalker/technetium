function getDeviceList()
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	local t = ALSoundManager:getDeviceList()
	linda:send("getDeviceList",t)
end

function newBitStream(filename,isStreamed)
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:newBitStream(filename,isStreamed)
end

function suspend()
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:suspend()
end

function resume()
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:resume()
end

function updateListener(x,y,z,vx,vy,vz,m1,m2,m3,m4,m5,m6)
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:updateListener(x,y,z,vx,vy,vz,m1,m2,m3,m4,m5,m6)
end

function playSound(alias,x,y,z,volume,loop,relative,mandatory)
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:playSound(alias,x,y,z,volume,loop,relative,mandatory)
end

function stopSound(alias)
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:stopSound(alias)
end

function getCurrentDevice()
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	local str = ALSoundManager:getCurrentDevice()
	linda:send("getCurrentDevice",str)
end

function setDevice(str)
	if not (ALSoundManager and ALSoundManager.initialized) then
		return
	end
	ALSoundManager:setDevice(str)
end

function reloadSoundManager()
	if (ALSoundManager) then
		ALSoundManager:__gc()
	end
	ALSoundManager = cSoundManager()
	if not (ALSoundManager.initialized) then
		ALSoundManager = nil
	end
end