Class "cCamera"
function cCamera:__init()
	local centering = 0
	local w,h = core.window.framebufferSize()
	
	self.properties = {
		position = ffi.new("vec3_t"),
		angle = ffi.new("vec3_t"),
		view = ffi.new("mat4_t"):identity(),																											-- Inverse of camera's model matrix
		projection = ffi.new("mat4_t"):from_ortho(centering,w+centering,h+centering,centering,0,100,0,core.renderer.is_homogeneous_depth(),false)		-- Projection matrix
	}
end

function cCamera:position(x,y,z)
	local pos = self.properties.position
	if (x or y or z) then
		pos.x = x or pos.x
		pos.y = y or pos.y
		pos.z = z or pos.z
	end
	return pos:unpack()
end

function cCamera:send(view_id)
	local pos = self.properties.position
	local view = self.properties.view
	local offset_mtx = view * view:translation(pos:unpack())
	core.renderer.set_view_transform(view_id or 0,offset_mtx,self.properties.projection)
end

function cCamera:move(x,y)
	local pos = self.properties.position
	self:position(pos.x+x,pos.y+y)
end

function cCamera:mousemoved(x,y,dx,dy)
	if (core.mouse.pressed(glfw.MOUSE_BUTTON_RIGHT)) then
		local spd = core.keyboard.pressed(glfw.KEY_LEFT_CONTROL) and 0.5 or 1
		self:move(math.ceil(dx*spd),math.ceil(dy*spd))
	end
end

function cCamera:wheelmoved(dx,dy)

end

function cCamera:mousepressed(x,y,button,istouch)

end

function cCamera:keyboard(key,aliases,scancode,action,mods)

end

function cCamera:rotate(z)
	self.transform.view:rotateZ(math.rad(z))
end

function cCamera:zoom(s)
	self.transform.view:scale(s,s,s)
end

return cCamera()