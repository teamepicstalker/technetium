--[[
		  (Z)
   (X) (Y) F   P
	L      R   O
	E   U  O   S
	F   P  N
	T	   T
	m0,m4,m8,m12
	m1,m5,m9,m13
	m2,m6,m10,m14
	m3,m7,m11,m15
--]]
Class "cCamera"
function cCamera:__init()
	self.type = "perspective"
	
	-- self.frustum = {
		-- planes = {
			-- ffi.new("kmPlane"),		-- Left
			-- ffi.new("kmPlane"),		-- Right
			-- ffi.new("kmPlane"),		-- Bottom
			-- ffi.new("kmPlane"),		-- Top
			-- ffi.new("kmPlane"),		-- Near
			-- ffi.new("kmPlane")		-- Far
		-- },
		-- corners = {
			-- ffi.new("kmVec3"),		-- Near Bottom Left
			-- ffi.new("kmVec3"),		-- Near Bottom Right
			-- ffi.new("kmVec3"),		-- Near Top Right
			-- ffi.new("kmVec3"),		-- Near Top Left
			-- ffi.new("kmVec3"),		-- Far Bottom Left
			-- ffi.new("kmVec3"),		-- Far Bottom Right
			-- ffi.new("kmVec3"),		-- Far Top Right
			-- ffi.new("kmVec3"),		-- Far Top Left
		-- }
	-- }

	local w,h = core.window.size()
	local fov, near, far, aspect = 60, 0.1, 100, w/h
	
	self.properties = {
		position 	= ffi.new("vec3_t"),
		angle		= ffi.new("vec3_t"),
		fov 		= fov,
		near 		= near,
		far 		= far,
		aspect		= aspect,
		view 		= ffi.new("mat4_t"):identity():look_at(ffi.new("vec3_t",0,0,-15),ffi.new("vec3_t",0,0,0),ffi.new("vec3_t",0,1,0),false),
		projection  = ffi.new("mat4_t"):from_perspective(fov,aspect,near,far,core.renderer.is_homogeneous_depth(),false)
	}
end

function cCamera:onWindowResize(w,h)
	self.properties.aspect = w/h
end

function cCamera:send(view_id)
	local pos = self.properties.position
	local ang = self.properties.angle
	local view = self.properties.view
	local offset_mtx = view * view:translation(pos:unpack())
	offset_mtx:rotateXYZ(ang:unpack())
	
	-- invert view matrix to get listener matrix
	local mdl_mtx = offset_mtx:inverse()
	core.audio.updateListener(mdl_mtx.r30,mdl_mtx.r31,mdl_mtx.r32,0,0,0,mdl_mtx.r10,mdl_mtx.r11,mdl_mtx.r12,mdl_mtx.r20,mdl_mtx.r21,mdl_mtx.r22)
	
	core.renderer.set_view_transform(view_id or 0,offset_mtx,self.properties.projection)
end

-- function cCamera:push()
	-- self:checkPropertyChanges()
	
	-- local projectionMatrix = gfx.transform("projection")
	-- kazmath.kmMat4Assign(projectionMatrix,self.projection)
	
	-- gfx.transform.mode("model")
	-- gfx.transform.push()
	
		-- local modelMatrix = gfx.transform("model")
		
		-- gfx.transform.translate(self.position[1],self.position[2],self.position[3])
		-- gfx.transform.rotate(self.orientation)
	
		-- -- Update OpenAL Listener with world-space coordinates of listener's location
		-- local mat = modelMatrix.mat
		-- core.audio.updateListener(self.position[1],self.position[2],self.position[3],0,0,0,mat[4],mat[5],mat[6],mat[8],mat[9],mat[10])
		
	-- gfx.transform.pop()
	
	-- gfx.transform.mode("view")
	-- gfx.transform.push()
	
		-- -- View Matrix is the inverse of the camera's model matrix
		-- self.viewMatrix = gfx.transform("view")
		-- kazmath.kmMat4Inverse(self.viewMatrix,modelMatrix)

		-- -- Rebuild Frustum
		-- local mvp = ffi.new("kmMat4")
		-- kazmath.kmMat4Multiply(mvp,projectionMatrix,self.viewMatrix)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[1],mvp,1)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[2],mvp,-1)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[3],mvp,2)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[4],mvp,-2)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[5],mvp,3)
		-- kazmath.kmPlaneExtractFromMat4(self.frustum.planes[6],mvp,-3)

		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[1],self.frustum.planes[1],self.frustum.planes[3],self.frustum.planes[5])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[2],self.frustum.planes[2],self.frustum.planes[3],self.frustum.planes[5])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[3],self.frustum.planes[2],self.frustum.planes[4],self.frustum.planes[5])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[4],self.frustum.planes[1],self.frustum.planes[4],self.frustum.planes[5])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[5],self.frustum.planes[1],self.frustum.planes[3],self.frustum.planes[6])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[6],self.frustum.planes[2],self.frustum.planes[3],self.frustum.planes[6])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[7],self.frustum.planes[2],self.frustum.planes[4],self.frustum.planes[6])
		-- kazmath.kmPlaneGetIntersection(self.frustum.corners[8],self.frustum.planes[1],self.frustum.planes[4],self.frustum.planes[6])
-- end

function cCamera:position(x,y,z)
	local pos = self.properties.position
	if (x or y or z) then
		pos.x = x or pos.x
		pos.y = y or pos.y
		pos.z = z or pos.z
	end
	return pos:unpack()
end

function cCamera:move(x,y,z)
	local pos = self.properties.position
	self:position(pos.x+x,pos.y+y,pos.z+z)
end

function cCamera:rotate(x,y,z)
	local ang = self.properties.angle
	ang(ang.x+x,ang.y+y,ang.z+z)
	ang.x = utils.wrap_minmax(ang.x,-2*math.pi,2*math.pi)
	ang.y = utils.wrap_minmax(ang.y,-2*math.pi,2*math.pi)
	ang.z = utils.wrap_minmax(ang.z,-2*math.pi,2*math.pi)
end

function cCamera:pitch(angle)
	self:rotate(angle,0,0)
end

function cCamera:yaw(angle)
	self:rotate(0,angle,0)
end

function cCamera:roll(angle)
	self:rotate(0,0,angle)
end

function cCamera:mousemoved(x,y,dx,dy)
	-- If both left and right mouse button press -- rotate!
	if (core.mouse.pressed(glfw.MOUSE_BUTTON_LEFT) and core.mouse.pressed(glfw.MOUSE_BUTTON_RIGHT)) then
		local spd = core.keyboard.pressed(glfw.KEY_LEFT_CONTROL) and 0.001 or 0.005
		self:rotate(-dy*spd,-dx*spd,0)
	-- If right mouse button pressed -- pan!
	elseif (core.mouse.pressed(glfw.MOUSE_BUTTON_RIGHT)) then
		local spd = core.keyboard.pressed(glfw.KEY_LEFT_CONTROL) and 0.01 or 0.1
		self:move(dx*spd,-dy*spd,0)	
	end
end

function cCamera:wheelmoved(dx,dy)
	local spd = core.keyboard.pressed(glfw.KEY_LEFT_CONTROL) and 0.5 or 1
	self:move(0,0,-dy*spd)
end

function cCamera:mousepressed(x,y,button)

end

function cCamera:keyboard(key,aliases,scancode,action,mods)

end

function cCamera:rayFromMouse()
	local mx,my = core.mouse.position()
	local vx,vy,vw,vh = core.renderer.get_view_rect()
	
	-- Calculate normalized device coordinates (NDC)
	local x = (((mx - vx) / vw) * 2 - 1) * self.properties.aspect
	local y = ((vh - my - vy) / vh) * 2 - 1
	
	-- Calculate the view ratio (tan(fov/2)) based on the camera's field of view.
	local viewRatio = math.tan(math.pi / (180/self.properties.fov) / 2)
	x = x * viewRatio
	y = y * viewRatio
	
	-- Calculate the x and y values in the camera's view space by multiplying the NDC coordinates with the view ratio.
	local n = self.properties.near
	local f = self.properties.far
	-- Invert the camera's view matrix to get the world-to-view matrix.
	local inverse = self.properties.view:inverse()
	-- Calculate the world space coordinates of the near and far clip planes using the world-to-view matrix and the near/far clip plane distances.
	local worldNear = inverse:mul_pos(x*n,y*n,-n)
	local worldFar = inverse:mul_pos(x*f,y*f,-f)
	-- Calculate the ray direction by subtracting the world space coordinates of the near and far clip planes and normalizing the resulting vector.
	local dir = ffi.new("vec3_t",worldFar.x-worldNear.x,worldFar.y-worldNear.y,worldFar.z-worldNear.z):normalize()
	
	-- Return the world space coordinates of the near clip plane and the ray direction.
	return worldNear.x,worldNear.y,worldNear.z,dir.x,dir.y,dir.z
end

function cCamera:pointInFrustum(x,y,z)
	-- TODO: Rewrite for bgfx
	for i,plane in ipairs(self.frustum.planes) do
		local dist = plane.a * x + plane.b * y + plane.c * z + plane.d
		if (dist < 0) then
			return false
		end
	end

	return true
end

function cCamera:sphereInFrustum(x,y,z,r)
	-- TODO: Rewrite for bgfx
	local dist
	for i,plane in ipairs(self.frustum.planes) do
		local dist = plane.a * x + plane.b * y + plane.c * z + plane.d
		if (dist < -r) then
			return false
		end
	end
	return dist + r
end

function cCamera:AABBInFrustum(minX,minY,minZ,maxX,maxY,maxZ)
	-- TODO: Rewrite for bgfx
	local bounds = ffi.new("kmVec3[6]",
		{maxX,minY,minZ},{minX,maxY,minZ},{maxX,maxY,minZ},
		{minX,minY,maxZ},{maxX,minY,maxZ},{minX,maxY,maxZ}
	)
	local vmin = ffi.new("kmVec3",minX,minY,minZ)
	local vmax = ffi.new("kmVec3",maxX,maxY,maxZ)
	for i,plane in ipairs(self.frustum.planes) do
		if (kazmath.kmPlaneDotCoord(plane,vmin) < 0) then
			if (kazmath.kmPlaneDotCoord(plane,bounds[0]) < 0) then
				if (kazmath.kmPlaneDotCoord(plane,bounds[1]) < 0) then
					if (kazmath.kmPlaneDotCoord(plane,bounds[2]) < 0) then
						if (kazmath.kmPlaneDotCoord(plane,bounds[3]) < 0) then
							if (kazmath.kmPlaneDotCoord(plane,bounds[4]) < 0) then
								if (kazmath.kmPlaneDotCoord(plane,bounds[5]) < 0) then
									if (kazmath.kmPlaneDotCoord(plane,vmax) < 0) then
										return false
									end
								end
							end
						end
					end
				end
			end
		end
	end
	local a,b,c,d,e,f = 0,0,0,0,0,0
	for i,corner in ipairs(self.frustum.corners) do
		a = corner.x > maxX and a + 1 or a
		b = corner.x < minX and b + 1 or b
		c = corner.y > maxY and c + 1 or c
		d = corner.y < minY and d + 1 or d
		e = corner.z > maxZ and e + 1 or e
		f = corner.z < minZ and f + 1 or f
	end
	if (a == 8 or b == 8 or c == 8 or d == 8 or e == 8 or f == 8) then
		return false
	end
	return true
end

function cCamera:drawFrustum()
	-- TODO: Rewrite for bgfx
	-- local corners = self.frustum.corners
	-- local vertices = ffi.new("GLfloat[?]",72,{
		-- corners[1].x, corners[1].y, corners[1].z,
		-- corners[2].x, corners[2].y, corners[2].z,
		-- corners[2].x, corners[2].y, corners[2].z,
		-- corners[3].x, corners[3].y, corners[3].z,
		-- corners[3].x, corners[3].y, corners[3].z,
		-- corners[4].x, corners[4].y, corners[4].z,
		-- corners[4].x, corners[4].y, corners[4].z,
		-- corners[1].x, corners[1].y, corners[1].z,

		-- corners[5].x, corners[5].y, corners[5].z,
		-- corners[6].x, corners[6].y, corners[6].z,
		-- corners[6].x, corners[6].y, corners[6].z,
		-- corners[7].x, corners[7].y, corners[7].z,
		-- corners[7].x, corners[7].y, corners[7].z,
		-- corners[8].x, corners[8].y, corners[8].z,
		-- corners[8].x, corners[8].y, corners[8].z,
		-- corners[5].x, corners[5].y, corners[5].z,

		-- corners[1].x, corners[1].y, corners[1].z,
		-- corners[5].x, corners[5].y, corners[5].z,
		-- corners[2].x, corners[2].y, corners[2].z,
		-- corners[6].x, corners[6].y, corners[6].z,
		-- corners[3].x, corners[3].y, corners[3].z,
		-- corners[7].x, corners[7].y, corners[7].z,
		-- corners[4].x, corners[4].y, corners[4].z,
		-- corners[8].x, corners[8].y, corners[8].z  
	-- })

	-- local defaultVBO = gfx.getDefaultVBO()
	-- GL.glBindBuffer(GL.GL_ARRAY_BUFFER,defaultVBO[0])
	-- GL.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(vertices),vertices,GL.GL_STATIC_DRAW)
	
	-- local position = gfx.shaders.getAttribLocation("position")
	-- local texcoord = gfx.shaders.getAttribLocation("texcoord")
	-- local normal = gfx.shaders.getAttribLocation("normal")
	-- local tangent = gfx.shaders.getAttribLocation("tangent")
	-- local bitangent = gfx.shaders.getAttribLocation("bitangent")
	
	-- GL.glEnableVertexAttribArray(position)
	-- GL.glDisableVertexAttribArray(texcoord)
	-- GL.glDisableVertexAttribArray(normal)
	-- GL.glDisableVertexAttribArray(tangent)
	-- GL.glDisableVertexAttribArray(bitangent)
	
	-- GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,0,nil)

	-- gfx.transform.updateTransformations()
	-- gfx.drawArrays(GL.GL_LINES,0,24)
end

return cCamera()