local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end

cache = {} -- for xml data
_curStyle = nil

-- core.interface.import.style(path)
getmetatable(this).__call = function(self,path)
	local ctx = core.interface.backend.device.context[ 0 ]
	nk.nk_style_default(ctx)
	if (path) then
		set_style(path,ctx)
	end
	_curStyle = path
end

----------------------------------------
-- Helpers
----------------------------------------
local defines = {} -- Temporary 'defines' to replace text inline
								
function getColor(str)
	str = defines[str] or str
	local r,g,b,a = 0,0,0,255
	if (str) then
		r,g,b,a = string.match(str,"([%d%s]*),([%d%s]*),([%d%s]*),([%d%s]*)")
		r = tonumber(r) or 0
		g = tonumber(g) or 0
		b = tonumber(b) or 0
		a = tonumber(a) or 255
	end
	return nk.nk_rgba(r,g,b,a)
end

function getVec2(str)
	str = defines[str] or str
	local x,y = string.match(str,"([%d%s]*),([%d%s]*)")
	return nk.nk_vec2(tonumber(x) or 0,tonumber(y) or 0)
end

function getFlags(str)
	str = defines[str] or str
	local flag = 0
	if (str) then
		--for s in str:gsplit("|",true) do
		for s in str:gmatch("([^|]+)") do
			if (nk[s]) then
				flag = bit.bor(flag,nk[s])
			end
		end
	end
	return flag
end

function getEnum(str)
	str = defines[str] or str
	return nk[str]
end

function getValue(str)
	str = defines[str] or str
	return tonumber(str) or 0
end 

function getBool(str,ptr)
	str = defines[str] or str
	return str == "true" and 1 or 0
end

local image_cache = {}
function getStyleItem(str,nineslice)
	str = defines[str] or str

	if (string.find(str,",") ~= nil) then
		return nk.nk_style_item_color(getColor(str))
	end
	
	if (nineslice) then
		local image = image_cache[str] or core.import.image.animation.exists(str) and core.import.image.animation(str) or core.import.image(str)
		if (image) then
			local img = ffi.new("struct nk_image")
			img.handle = nk.nk_handle_id(image:getTextureID())
			local w,h = image:getTextureSize()
			img.w = w
			img.h = h
			img.region = ffi.new("unsigned short[4]",{image:getRegion()})
			
			local l,t,r,b = string.match(nineslice,"([%d%s]*),([%d%s]*),([%d%s]*),([%d%s]*)")
			local slice = ffi.new("struct nk_nine_slice")
			slice.img = img
			slice.l = tonumber(l) or 0
			slice.t = tonumber(t) or 0
			slice.r = tonumber(r) or 0
			slice.b = tonumber(b) or 0
			
			return nk.nk_style_item_nine_slice(slice)
		end		
		return nk.nk_style_item_color(nk.nk_rgba(0,0,0,255))
	end
	
	local image = image_cache[str] or core.import.image.animation.exists(str) and core.import.image.animation(str) or core.import.image(str)
	
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		local w,h = image:getTextureSize()
		img.w = w
		img.h = h
		img.region = ffi.new("unsigned short[4]",{image:getRegion()})
		return nk.nk_style_item_image(img)
	end
	return nk.nk_style_item_color(nk.nk_rgba(0,0,0,255))
end 

---------------------------
-- Style from XML
---------------------------
function set_style(path,ctx)
	-- Load and cache xml data
	local node = cache[path]
	if not (node) then
		local xml = core.import.xml(path)
		assert(xml,"can't load " .. path)
		node = xml.data
		cache[path] = node
	end
	
	-- Defines are for inline string replacements for simplicity sake
	if (node.defines) then
		for k,v in pairs(node.defines[ 1 ]) do
			if (v[ 1 ]) then
				local val = v[ 1 ][ 2 ] and utils.trim(v[ 1 ][ 2 ]) or ""
				val = val ~= "" and val or nil
				defines[ k ] = val -- transfer value to table
			end
		end
	end
	
	-- Remap colors of default style
	if (node.color_table) then
		local color_order = {
			"COLOR_TEXT",
			"COLOR_WINDOW",
			"COLOR_HEADER",
			"COLOR_BORDER",
			"COLOR_BUTTON",
			"COLOR_BUTTON_HOVER",
			"COLOR_BUTTON_ACTIVE",
			"COLOR_TOGGLE",
			"COLOR_TOGGLE_HOVER",
			"COLOR_TOGGLE_CURSOR",
			"COLOR_SELECT",
			"COLOR_SELECT_ACTIVE",
			"COLOR_SLIDER",
			"COLOR_SLIDER_CURSOR",
			"COLOR_SLIDER_CURSOR_HOVER",
			"COLOR_SLIDER_CURSOR_ACTIVE",
			"COLOR_PROPERTY",
			"COLOR_EDIT",
			"COLOR_EDIT_CURSOR",
			"COLOR_COMBO",
			"COLOR_CHART",
			"COLOR_CHART_COLOR",
			"COLOR_CHART_COLOR_HIGHLIGHT",
			"COLOR_SCROLLBAR",
			"COLOR_SCROLLBAR_CURSOR",
			"COLOR_SCROLLBAR_CURSOR_HOVER",
			"COLOR_SCROLLBAR_CURSOR_ACTIVE",
			"COLOR_TAB_HEADER"
		}
		
		local color_map = {}
		for i,section in ipairs(color_order) do
			local attribute = node.color_table[ 1 ][ section ] and node.color_table[ 1 ][ section ][ 1 ]
			if (attribute) then
				local str = utils.trim(attribute[ 2 ])
				if (str ~= "") then
					local r,g,b,a = string.match(str,"([%d%s]*),([%d%s]*),([%d%s]*),([%d%s]*)")
					table.insert(color_map,{tonumber(r) or 0,tonumber(g) or 0,tonumber(b) or 0,tonumber(a) or 255})
				end
			end
		end

		set_style_from_table(color_map)
		return
	end
	
	local style = ctx.style
	-- default text
	if (node.text) then
		local attrib = node.text[ 1 ]
		local ptr = style.text
		ptr.color = getColor(attrib.color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
	end
	
	-- default button
	local function parse_button(attrib,ptr)
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_button"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.text_background = getColor(attrib.text_background[ 1 ][ 2 ])
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_active = getColor(attrib.text_active[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.image_padding = getVec2(attrib.image_padding[ 1 ][ 2 ])
		ptr.touch_padding = getVec2(attrib.touch_padding[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
	end
	if (node.button) then
		local attrib = node.button[ 1 ]
		local ptr = style.button	
		parse_button(attrib,ptr)
	end
	
	-- contextual button
	if (node.contextual_button) then
		local attrib = node.contextual_button[ 1 ]
		local ptr = style.contextual_button	
		parse_button(attrib,ptr)	
	end

	-- menu button
	if (node.menu_button) then
		local attrib = node.menu_button[ 1 ]
		local ptr = style.menu_button	
		parse_button(attrib,ptr)	
	end
	
	-- checkbox
	if (node.checkbox) then
		local attrib = node.checkbox[ 1 ]
		local ptr = style.checkbox
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_toggle"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.text_background = getColor(attrib.text_background[ 1 ][ 2 ])
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_active = getColor(attrib.text_active[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.touch_padding = getVec2(attrib.touch_padding[ 1 ][ 2 ])
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.spacing = getValue(attrib.spacing[ 1 ][ 2 ])
	end

	-- option
	if (node.option) then
		local attrib = node.option[ 1 ]
		local ptr = style.option
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_toggle"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.text_background = getColor(attrib.text_background[ 1 ][ 2 ])
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_active = getColor(attrib.text_active[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.touch_padding = getVec2(attrib.touch_padding[ 1 ][ 2 ])
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.spacing = getValue(attrib.spacing[ 1 ][ 2 ])
	end
	
	-- selectable
	if (node.selectable) then
		local attrib = node.selectable[ 1 ]
		local ptr = style.selectable
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_selectable"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.pressed = getStyleItem(attrib.pressed[ 1 ][ 2 ], attrib.pressed[ 1 ][ 1 ].nineslice)
		ptr.normal_active = getStyleItem(attrib.normal_active[ 1 ][ 2 ], attrib.normal_active[ 1 ][ 1 ].nineslice)
		ptr.hover_active = getStyleItem(attrib.hover_active[ 1 ][ 2 ], attrib.hover_active[ 1 ][ 1 ].nineslice)
		ptr.pressed_active = getStyleItem(attrib.pressed_active[ 1 ][ 2 ], attrib.pressed_active[ 1 ][ 1 ].nineslice)
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_pressed = getColor(attrib.text_pressed[ 1 ][ 2 ])
		ptr.text_normal_active = getColor(attrib.text_normal_active[ 1 ][ 2 ])
		ptr.text_hover_active = getColor(attrib.text_hover_active[ 1 ][ 2 ])
		ptr.text_pressed_active = getColor(attrib.text_pressed_active[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.image_padding = getVec2(attrib.image_padding[ 1 ][ 2 ])
		ptr.touch_padding = getVec2(attrib.touch_padding[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
	end
	
	-- slider
	if (node.slider) then
		local attrib = node.slider[ 1 ]
		local ptr = style.slider
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_slider"))
		ptr.normal = nk.nk_style_item_hide()
		ptr.hover = nk.nk_style_item_hide()
		ptr.active = nk.nk_style_item_hide()
		ptr.bar_normal = getColor(attrib.bar_normal[ 1 ][ 2 ])
		ptr.bar_hover = getColor(attrib.bar_hover[ 1 ][ 2 ])
		ptr.bar_active = getColor(attrib.bar_active[ 1 ][ 2 ])
		ptr.bar_filled = getColor(attrib.bar_filled[ 1 ][ 2 ])
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ])
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ])
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ])
		ptr.inc_symbol = getEnum(attrib.inc_symbol[ 1 ][ 2 ])
		ptr.dec_symbol = getEnum(attrib.dec_symbol[ 1 ][ 2 ])
		ptr.cursor_size = getVec2(attrib.cursor_size[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.spacing = getVec2(attrib.spacing[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.show_buttons = getBool(attrib.show_buttons[ 1 ][ 2 ])
		ptr.bar_height = getValue(attrib.bar_height[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.slider[ 1 ].inc_button[ 1 ]
		ptr = style.slider.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.slider[ 1 ].dec_button[ 1 ]
		ptr = style.slider.dec_button
		parse_button(attrib,ptr)
	end
	
	-- progress bar
	if (node.progress) then
		local attrib = node.progress[ 1 ]
		local ptr = style.progress
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_progress"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ])
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ])
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ])
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.cursor_border_color = getColor(attrib.cursor_border_color[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.cursor_rounding = getValue(attrib.cursor_rounding[ 1 ][ 2 ])
		ptr.cursor_border = getValue(attrib.cursor_border[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
	end
	
	--scrollbar vertical
	if (node.scrollv) then
		local attrib = node.scrollv[ 1 ]
		local ptr = style.scrollv
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_scrollbar"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ], attrib.cursor_active[ 1 ][ 1 ].nineslice)
		ptr.inc_symbol = getEnum(attrib.inc_symbol[ 1 ][ 2 ])
		ptr.dec_symbol = getEnum(attrib.dec_symbol[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.cursor_border_color = getColor(attrib.cursor_border_color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.show_buttons = getBool(attrib.show_buttons[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.border_cursor = getValue(attrib.border_cursor[ 1 ][ 2 ])
		ptr.rounding_cursor = getValue(attrib.rounding_cursor[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.scrollv[ 1 ].inc_button[ 1 ]
		ptr = style.scrollv.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.scrollv[ 1 ].dec_button[ 1 ]
		ptr = style.scrollv.dec_button
		parse_button(attrib,ptr)
	end

	-- scrollbar horizontal
	if (node.scrollh) then
		local attrib = node.scrollh[ 1 ]
		local ptr = style.scrollh
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_scrollbar"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ], attrib.cursor_active[ 1 ][ 1 ].nineslice)
		ptr.inc_symbol = getEnum(attrib.inc_symbol[ 1 ][ 2 ])
		ptr.dec_symbol = getEnum(attrib.dec_symbol[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.cursor_border_color = getColor(attrib.cursor_border_color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.show_buttons = getBool(attrib.show_buttons[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.border_cursor = getValue(attrib.border_cursor[ 1 ][ 2 ])
		ptr.rounding_cursor = getValue(attrib.rounding_cursor[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.scrollh[ 1 ].inc_button[ 1 ]
		ptr = style.scrollh.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.scrollh[ 1 ].dec_button[ 1 ]
		ptr = style.scrollh.dec_button
		parse_button(attrib,ptr)
	end
	
	-- edit
	if (node.edit) then
		local attrib = node.edit[ 1 ]
		local ptr = style.edit
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_edit"))
	    ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getColor(attrib.cursor_normal[ 1 ][ 2 ])
		ptr.cursor_hover = getColor(attrib.cursor_hover[ 1 ][ 2 ])
		ptr.cursor_text_normal = getColor(attrib.cursor_text_normal[ 1 ][ 2 ])
		ptr.cursor_text_hover = getColor(attrib.cursor_text_hover[ 1 ][ 2 ])
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_active = getColor(attrib.text_active[ 1 ][ 2 ])
		ptr.selected_normal = getColor(attrib.selected_normal[ 1 ][ 2 ])
		ptr.selected_hover = getColor(attrib.selected_hover[ 1 ][ 2 ])
		ptr.selected_text_normal = getColor(attrib.selected_text_normal[ 1 ][ 2 ])
		ptr.selected_text_hover = getColor(attrib.selected_text_hover[ 1 ][ 2 ])
		ptr.scrollbar_size= getVec2(attrib.scrollbar_size[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.row_padding = getValue(attrib.row_padding[ 1 ][ 2 ])
		ptr.cursor_size = getValue(attrib.cursor_size[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		-- scrollbar
		attrib = node.edit[ 1 ].scrollbar[ 1 ]
		ptr = style.edit.scrollbar
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_scrollbar"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ], attrib.cursor_active[ 1 ][ 1 ].nineslice)
		ptr.inc_symbol = getEnum(attrib.inc_symbol[ 1 ][ 2 ])
		ptr.dec_symbol = getEnum(attrib.dec_symbol[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.cursor_border_color = getColor(attrib.cursor_border_color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.show_buttons = getBool(attrib.show_buttons[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.border_cursor = getValue(attrib.border_cursor[ 1 ][ 2 ])
		ptr.rounding_cursor = getValue(attrib.rounding_cursor[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.edit[ 1 ].scrollbar[ 1 ].inc_button[ 1 ]
		ptr = style.edit.scrollbar.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.edit[ 1 ].scrollbar[ 1 ].dec_button[ 1 ]
		ptr = style.edit.scrollbar.dec_button
		parse_button(attrib,ptr)
	end
	
	if (node.edit_scroll) then

	end
	
	if (node.edit_scroll_inc) then
		local attrib = node.edit_scroll_inc[ 1 ]
		local ptr = style.edit.scrollbar.inc_button
		parse_button(attrib,ptr)
	end

	if (node.edit_scroll_dec) then
		local attrib = node.edit_scroll_dec[ 1 ]
		local ptr = style.edit.scrollbar.dec_button
		parse_button(attrib,ptr)
	end
	
	-- property
	if (node.property) then
		local attrib = node.property[ 1 ]
		local ptr = style.property
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_property"))
	    ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.label_normal = getColor(attrib.label_normal[ 1 ][ 2 ])
		ptr.label_hover = getColor(attrib.label_hover[ 1 ][ 2 ])
		ptr.label_active = getColor(attrib.label_active[ 1 ][ 2 ])
		ptr.sym_left = getEnum(attrib.sym_left[ 1 ][ 2 ]);
		ptr.sym_right = getEnum(attrib.sym_right[ 1 ][ 2 ]);
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.property[ 1 ].inc_button[ 1 ]
		ptr = style.property.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.property[ 1 ].dec_button[ 1 ]
		ptr = style.property.dec_button
		parse_button(attrib,ptr)
		
		-- property edit
		attrib = node.property[ 1 ].edit[ 1 ]
		ptr = style.property.edit
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_edit"))
	    ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getColor(attrib.cursor_normal[ 1 ][ 2 ])
		ptr.cursor_hover = getColor(attrib.cursor_hover[ 1 ][ 2 ])
		ptr.cursor_text_normal = getColor(attrib.cursor_text_normal[ 1 ][ 2 ])
		ptr.cursor_text_hover = getColor(attrib.cursor_text_hover[ 1 ][ 2 ])
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.text_normal = getColor(attrib.text_normal[ 1 ][ 2 ])
		ptr.text_hover = getColor(attrib.text_hover[ 1 ][ 2 ])
		ptr.text_active = getColor(attrib.text_active[ 1 ][ 2 ])
		ptr.selected_normal = getColor(attrib.selected_normal[ 1 ][ 2 ])
		ptr.selected_hover = getColor(attrib.selected_hover[ 1 ][ 2 ])
		ptr.selected_text_normal = getColor(attrib.selected_text_normal[ 1 ][ 2 ])
		ptr.selected_text_hover = getColor(attrib.selected_text_hover[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.row_padding = getValue(attrib.row_padding[ 1 ][ 2 ])
		ptr.cursor_size = getValue(attrib.cursor_size[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		
		-- property edit scrollbar
		attrib = node.property[ 1 ].edit[ 1 ].scrollbar[ 1 ]
		ptr = style.edit.scrollbar
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_scrollbar"))
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.cursor_normal = getStyleItem(attrib.cursor_normal[ 1 ][ 2 ], attrib.cursor_normal[ 1 ][ 1 ].nineslice)
		ptr.cursor_hover = getStyleItem(attrib.cursor_hover[ 1 ][ 2 ], attrib.cursor_hover[ 1 ][ 1 ].nineslice)
		ptr.cursor_active = getStyleItem(attrib.cursor_active[ 1 ][ 2 ], attrib.cursor_active[ 1 ][ 1 ].nineslice)
		ptr.inc_symbol = getEnum(attrib.inc_symbol[ 1 ][ 2 ])
		ptr.dec_symbol = getEnum(attrib.dec_symbol[ 1 ][ 2 ])
		ptr.userdata = nk.nk_handle_ptr(nil)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.cursor_border_color = getColor(attrib.cursor_border_color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.show_buttons = getBool(attrib.show_buttons[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.border_cursor = getValue(attrib.border_cursor[ 1 ][ 2 ])
		ptr.rounding_cursor = getValue(attrib.rounding_cursor[ 1 ][ 2 ])
		ptr.draw_begin = nil
		ptr.draw_end = nil
		
		attrib = node.property[ 1 ].edit[ 1 ].scrollbar[ 1 ].inc_button[ 1 ]
		ptr = style.property.edit.scrollbar.inc_button
		parse_button(attrib,ptr)
		
		attrib = node.property[ 1 ].edit[ 1 ].scrollbar[ 1 ].dec_button[ 1 ]
		ptr = style.property.edit.scrollbar.dec_button
		parse_button(attrib,ptr)
	end
	
	-- property button increment
	if (node.property_inc) then
		local attrib = node.property_inc[ 1 ]
		local ptr = style.property.inc_button
		parse_button(attrib,ptr)
	end
	
	-- property button decrement
	if (node.property_dec) then
		local attrib = node.property_dec[ 1 ]
		local ptr = style.property.dec_button
		parse_button(attrib,ptr)
	end
	
	-- property edit
	if (node.property_edit) then

	end
	
	-- chart
	if (node.chart) then
		-- ffi.fill(ptr,ffi.sizeof("struct nk_style_chart"))
		local attrib = node.chart[ 1 ]
		local ptr = style.chart
		ptr.background = getStyleItem(attrib.background[ 1 ][ 2 ], attrib.background[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.selected_color = getColor(attrib.selected_color[ 1 ][ 2 ])
		ptr.color = getColor(attrib.color[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
	end
	
	-- combo
	if (node.combo) then
		local attrib = node.combo[ 1 ]
		local ptr = style.combo
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.label_normal = getColor(attrib.label_normal[ 1 ][ 2 ])
		ptr.label_hover = getColor(attrib.label_hover[ 1 ][ 2 ])
		ptr.label_active = getColor(attrib.label_active[ 1 ][ 2 ])
		ptr.sym_normal = getEnum(attrib.sym_normal[ 1 ][ 2 ]);
		ptr.sym_hover = getEnum(attrib.sym_hover[ 1 ][ 2 ]);
		ptr.sym_active = getEnum(attrib.sym_active[ 1 ][ 2 ]);
		ptr.content_padding = getVec2(attrib.content_padding[ 1 ][ 2 ])
		ptr.button_padding = getVec2(attrib.button_padding[ 1 ][ 2 ])
		ptr.spacing = getVec2(attrib.spacing[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
	end
	
	-- combo button
	if (node.combo_button) then
		local attrib = node.combo_button[ 1 ]
		local ptr = style.combo.button
		parse_button(attrib,ptr)	
	end
	
	-- tab
	if (node.tab) then
		local attrib = node.tab[ 1 ]
		local ptr = style.tab
		ptr.background = getStyleItem(attrib.background[ 1 ][ 2 ], attrib.background[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.text = getColor(attrib.text[ 1 ][ 2 ])
		ptr.sym_minimize = getEnum(attrib.sym_minimize[ 1 ][ 2 ])
		ptr.sym_maximize = getEnum(attrib.sym_maximize[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.spacing = getVec2(attrib.spacing[ 1 ][ 2 ])
		ptr.indent = getValue(attrib.indent[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
	end
	
	-- tab minimize button
	if (node.tab_minimize) then
		local attrib = node.tab_minimize[ 1 ]
		local ptr = style.tab.tab_minimize_button	
		parse_button(attrib,ptr)	
	end

	-- tab maximize button
	if (node.tab_maximize) then
		local attrib = node.tab_maximize[ 1 ]
		local ptr = style.tab.tab_maximize_button	
		parse_button(attrib,ptr)	
	end
	
	-- tab node minimize button
	if (node.tab_node_minimize) then
		local attrib = node.tab_node_minimize[ 1 ]
		local ptr = style.tab.node_minimize_button	
		parse_button(attrib,ptr)	
	end

	-- tab node maximize button
	if (node.tab_node_maximize) then
		local attrib = node.tab_node_maximize[ 1 ]
		local ptr = style.tab.node_maximize_button	
		parse_button(attrib,ptr)	
	end
	
	-- window
	if (node.window) then
		local attrib = node.window[ 1 ]
		local ptr = style.window
	    ptr.background = getColor(attrib.background[ 1 ][ 2 ])
		ptr.fixed_background = getStyleItem(attrib.fixed_background[ 1 ][ 2 ], attrib.fixed_background[ 1 ][ 1 ].nineslice)
		ptr.border_color = getColor(attrib.border_color[ 1 ][ 2 ])
		ptr.popup_border_color = getColor(attrib.popup_border_color[ 1 ][ 2 ])
		ptr.combo_border_color = getColor(attrib.combo_border_color[ 1 ][ 2 ])
		ptr.contextual_border_color = getColor(attrib.contextual_border_color[ 1 ][ 2 ])
		ptr.menu_border_color = getColor(attrib.menu_border_color[ 1 ][ 2 ])
		ptr.group_border_color = getColor(attrib.group_border_color[ 1 ][ 2 ])
		ptr.tooltip_border_color = getColor(attrib.tooltip_border_color[ 1 ][ 2 ])
		ptr.scaler = getStyleItem(attrib.scaler[ 1 ][ 2 ], attrib.scaler[ 1 ][ 1 ].nineslice)
		ptr.rounding = getValue(attrib.rounding[ 1 ][ 2 ])
		ptr.spacing = getVec2(attrib.spacing[ 1 ][ 2 ])
		ptr.scrollbar_size = getVec2(attrib.scrollbar_size[ 1 ][ 2 ])
		ptr.min_size = getVec2(attrib.min_size[ 1 ][ 2 ])
		ptr.combo_border = getValue(attrib.combo_border[ 1 ][ 2 ])
		ptr.contextual_border = getValue(attrib.contextual_border[ 1 ][ 2 ])
		ptr.menu_border = getValue(attrib.menu_border[ 1 ][ 2 ])
		ptr.group_border = getValue(attrib.group_border[ 1 ][ 2 ])
		ptr.tooltip_border = getValue(attrib.tooltip_border[ 1 ][ 2 ])
		ptr.popup_border = getValue(attrib.popup_border[ 1 ][ 2 ])
		ptr.border = getValue(attrib.border[ 1 ][ 2 ])
		ptr.min_row_height_padding = getValue(attrib.min_row_height_padding[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.group_padding = getVec2(attrib.group_padding[ 1 ][ 2 ])
		ptr.popup_padding = getVec2(attrib.popup_padding[ 1 ][ 2 ])
		ptr.combo_padding = getVec2(attrib.combo_padding[ 1 ][ 2 ])
		ptr.contextual_padding = getVec2(attrib.contextual_padding[ 1 ][ 2 ])
		ptr.menu_padding = getVec2(attrib.menu_padding[ 1 ][ 2 ])
		ptr.tooltip_padding = getVec2(attrib.tooltip_padding[ 1 ][ 2 ])
		
		-- window header
		attrib = node.window[ 1 ].header[ 1 ]
		ptr = style.window.header
		ptr.align = getEnum(attrib.align[ 1 ][ 2 ])
		ptr.close_symbol = getEnum(attrib.close_symbol[ 1 ][ 2 ])
		ptr.minimize_symbol = getEnum(attrib.minimize_symbol[ 1 ][ 2 ])
		ptr.maximize_symbol = getEnum(attrib.maximize_symbol[ 1 ][ 2 ])
		ptr.normal = getStyleItem(attrib.normal[ 1 ][ 2 ], attrib.normal[ 1 ][ 1 ].nineslice)
		ptr.hover = getStyleItem(attrib.hover[ 1 ][ 2 ], attrib.hover[ 1 ][ 1 ].nineslice)
		ptr.active = getStyleItem(attrib.active[ 1 ][ 2 ], attrib.active[ 1 ][ 1 ].nineslice)
		ptr.label_normal = getColor(attrib.label_normal[ 1 ][ 2 ])
		ptr.label_hover = getColor(attrib.label_hover[ 1 ][ 2 ])
		ptr.label_active = getColor(attrib.label_active[ 1 ][ 2 ])
		ptr.label_padding = getVec2(attrib.label_padding[ 1 ][ 2 ])
		ptr.padding = getVec2(attrib.padding[ 1 ][ 2 ])
		ptr.spacing = getVec2(attrib.spacing[ 1 ][ 2 ])
		
		-- window close button
		attrib = node.window[ 1 ].close_button[ 1 ]
		ptr = style.window.header.close_button
		parse_button(attrib,ptr)

		
		-- window minimize button
		attrib = node.window[ 1 ].minimize_button[ 1 ]
		ptr = style.window.header.minimize_button
		parse_button(attrib,ptr)
	end
end

local default_color_table = {	
		{175,175,175,255},			-- NK_COLOR_TEXT
		{45,45,45,255},				-- NK_COLOR_WINDOW
		{40,40,40,255},				-- NK_COLOR_HEADER
		{65,65,65,255},				-- NK_COLOR_BORDER
		{50,50,50,255},				-- NK_COLOR_BUTTON
		{40,40,40,255},				-- NK_COLOR_BUTTON_HOVER
		{35,35,35,255},				-- NK_COLOR_BUTTON_ACTIVE
		{100,100,100,255},			-- NK_COLOR_TOGGLE
		{120,120,120,255},			-- NK_COLOR_TOGGLE_HOVER
		{45,45,45,255},				-- NK_COLOR_TOGGLE_CURSOR
		{45,45,45,255},				-- NK_COLOR_SELECT
		{35,35,35,255},				-- NK_COLOR_SELECT_ACTIVE
		{38,38,38,255},				-- NK_COLOR_SLIDER
		{100,100,100,255},			-- NK_COLOR_SLIDER_CURSOR
		{120,120,120,255},			-- NK_COLOR_SLIDER_CURSOR_HOVER
		{150,150,150,255},			-- NK_COLOR_SLIDER_CURSOR_ACTIVE
		{38,38,38,38,255},			-- NK_COLOR_PROPERTY
		{38,38,38,38,255},			-- NK_COLOR_EDIT
		{175,175,175,255},			-- NK_COLOR_EDIT_CURSOR
		{45,45,45,255},				-- NK_COLOR_COMBO
		{120,120,120,255},			-- NK_COLOR_CHART
		{45,45,45,255},				-- NK_COLOR_CHART_COLOR
		{255,0,0,255},				-- NK_COLOR_CHART_COLOR_HIGHLIGHT
		{40,40,40,255},				-- NK_COLOR_SCROLLBAR
		{100,100,100,255},			-- NK_COLOR_SCROLLBAR_CURSOR
		{120,120,120,255},			-- NK_COLOR_SCROLLBAR_CURSOR_HOVER
		{150,150,150,255},			-- NK_COLOR_SCROLLBAR_CURSOR_ACTIVE
		{40,40,40,255}				-- NK_COLOR_TAB_HEADER
	}

function set_style_from_table(color_table)
	local ctx = core.interface.backend.device.context[ 0 ]
	local color_map = ffi.new("struct nk_color[28]",color_table or default_color_table)
	nk.nk_style_from_table(ctx,color_map)
end