cache					= {}		-- prefab by alias
local curID				= 0			-- Default ID of element if undefined in prefab

-- core.interface.import.prefab(alias,path,args)
getmetatable(this).__call = function(self,alias,path,args)
	local data = { __info={}, __args=args or {}, __alias=alias, __scriptEnv=nil }

	-- Can either load *.xml directly or a folder that contains *.xml by the same name
	if (filesystem.isDirectory(path)) then
		local fname = path:gsub("(.*[\\/])",""):gsub("(%..*)","")
		local xml_path = path .. "/" .. fname .. ".xml"
		if (filesystem.exists(xml_path) and not filesystem.isDirectory(xml_path)) then
			process_as_xml(alias,xml_path,data)
		end
		local script_path = path .. "/" .. fname .. ".lua"
		process_script(alias,script_path,data)
	elseif (filesystem.exists(path)) then
		local ext = utils.get_ext(path)
		if (ext == "xml") then 
			process_as_xml(alias,path,data)
		end
	end
	
	-- call initializer for optional script
	if (data.__scriptEnv and data.__scriptEnv.init) then
		data.__scriptEnv.init(data)
	end
		
	cache[ alias ] = data
	return data
end

function exists(alias)
	return cache[alias] ~= nil
end

function remove(alias)
	core.interface.api.unregister(alias)
end

-------------------------------------------
-- Helpers
-------------------------------------------
function process_as_xml(alias,path,data)
	local depth = 0
	local stack = {}
	local api = core.interface.api
	api.register(alias,stack)
	
	local xml = core.import.xml(path,nil,function(node,parent,element)
			local functor = api[ element ]
			if (functor) then
				table.insert(stack,{functor,node[ 1 ],data,depth})
			end
			depth = depth + 1
		end,function(node,parent,element)
			depth = depth - 1
			node[ 1 ].__content = node[ 2 ] -- simplify params
			if not (node[ 1 ].id) then
				node[ 1 ].id = "element" .. tostring(curID) -- If 'id' field is undefined assign an id
				curID = curID + 1
			end
			local functor = api[ element.."_end" ]
			if (functor) then
				table.insert(stack,{functor,node[ 1 ],data,depth})
			end
		end
	)

	-- Load script if <script> tag exists
	local node = xml.data.script
	if (node) then
		process_script(node[ 1 ].name,data)
	end
end

function process_script(alias,fname,data)
	if (filesystem.exists(fname) and not filesystem.isDirectory(fname)) then
		local old_package_path = package.path -- Because we don't want anywhere else to accidentally require scripts in prefab directories
		package.path = package.path .. ";" .. utils.get_path(fname) .. "?.lua"
		
		data.__scriptEnv = core.interface.import.script(fname)
		
		package.path = old_package_path
	end
end