local PlaceholderScene = {}

local batched_sprites = nil
local batched_tilesets = nil

function framework_run()
	CallbackSet("framework_update",framework_update)
	CallbackSet("framework_render",framework_render)
	CallbackSet("framework_load",framework_load)
	CallbackSet("framework_mousemoved",framework_mousemoved)
	CallbackSet("framework_wheelmoved",framework_wheelmoved)
	CallbackSet("framework_keyboard",framework_keyboard)
	CallbackSet("framework_resize",framework_resize)
end

function framework_load()
	core.interface.import.prefab("example02_editor","prefab/ui/editor.ui")

	-- load the scene map
	load_map("prefab/map/01.lua")
	
	-- load a camera prefab for the scene
	core.camera("default2D","prefab/camera/default_orthographic.lua")
	local ent = game.entity.create("spatial","sprite","persistable","player")
	ent.alias = "toader"
	ent.component.sprite:image("toader_hop_front")
	ent.component.spatial:position(640,640)
	
	-- for i=1,100 do
		-- local enemy = game.entity.create("spatial","sprite","persistable")
		-- enemy.alias = "enemy01"..i
		-- enemy.component.sprite:image(math.random(1,10) < 5 and "snake_idle_right" or "snake_idle_left")
		-- enemy.component.spatial:position(math.random(30)*64,math.random(20)*64)
	-- end
	
	batched_sprites = core.import.image.batch()
	batched_tilesets = core.import.image.batch()
end

function framework_mousemoved(x,y,dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:mousemoved(x,y,dx,dy)
	end
end

function framework_wheelmoved(dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:wheelmoved(dx,dy)
	end
end

function framework_keyboard(key,aliases,scancode,action,mods)
	for id, ent in pairs(game.entity.all()) do
		if (ent.component.player) then
			ent.component.player:keyboard(key,aliases,scancode,action,mods)
		end
	end
end

function framework_update(dt,mx,my)
	-- only rebuild tileset data if batch group is empty
	if (batched_tilesets:isEmpty() and batched_tilesets:isLoaded()) then
		local map = PlaceholderScene.map
		if (map and map.layers[ 1 ].data) then
			local tileset = map.tilesets[ 1 ].name
			
			local x,y = 0,0
			for i,v in ipairs(map.layers[ 1 ].data) do
				local alias = tileset .. "." .. v
			
				local image_import = core.import.image
				local image = image_import.animation.exists(alias) and image_import.animation(alias,true) or image_import.exists(alias) and image_import(alias)

				if (image) then
					local texture_info = image:getTextureInfo()
					if (texture_info and texture_info.loaded) then
						batched_tilesets:add(texture_info,x,y)
					end
				end
					
				x = x + map.tilewidth
				if (x >= map.width * map.tilewidth) then
					x = 0
					y = y + map.tileheight
				end
			end
		end
	end
	
	for id, ent in pairs(game.entity.all()) do
		for name,t in pairs(ent.component) do
			if (t.update) then
				t:update(dt,mx,my)
			end
		end
		
		if (ent.component.sprite) then
			local props = ent.component.sprite.properties
			if (props.visible) then
				local texture_info = ent.component.sprite:getTextureInfo()
				if (texture_info and texture_info.loaded) then
					local x,y = ent.component.spatial:position()
					batched_sprites:add(texture_info,x,y)
				end
			end
		end	
	end
end

function framework_resize(w,h)
	-- local map = PlaceholderScene.map
	-- local mw = map.width * map.tilewidth
	-- local mh = map.height * map.tileheight
	
	-- core.renderer.reset(mw,mh)
	-- bgfx.bgfx_set_view_rect(0,0,0,mw,mh)
end

function framework_render(winW,winH,fbW,fbH)
	local cam = core.camera.active("default2D")
	if not (cam) then
		return
	end

	local view_id = 0
	
	cam:send(view_id) -- Set view and project matrix for view_id 0
	
	batched_tilesets:flush(view_id)
	batched_sprites:flush(view_id)
	
	batched_sprites:clear()
end

function draw_map()

end

function load_map(fname)
	local chunk,err = filesystem.loadfile(fname)
	if (chunk == nil) then
		print(err,debug.traceback(1))
		return
	end
	
	local map = chunk()
	
	PlaceholderScene.map = map
	
	return map
end