--bgfx_create_frame_buffer
cache = {}

-- core.import.tiledmap(fname)
getmetatable(this).__call = function (self,alias,fname)
	local chunk,err = filesystem.loadfile(fname)
	if (chunk == nil) then
		print(err,debug.traceback(1))
		return
	end
	
	local tm = cTiledMap(fname)
	cache[ alias ] = tm
	tm.tmx = chunk()
end

------------------------------------------------------------------------
-- cTiledMap
------------------------------------------------------------------------
Class "cTiledMap"
function cTiledMap:__init(fname)
	self.framebuffer = bgfx_create_frame_buffer
end

function cTiledMap:renderToTexture()

end