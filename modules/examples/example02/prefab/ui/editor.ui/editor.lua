storage = nil

function editor_framework_render()
	if (storage.__info.ui_grid_active) then
		
	end
end

function editor_toolbar_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_toolbar") then
		local w,h = core.window.size()
		p.w = w
	elseif (node.id == "toggle_editor_win") then
		data.__info.ui_editor_win = p.active
	end
	
	storage = data
	CallbackSet("framework_render",editor_framework_render)
end

function editor_toolbar_action(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_toolbar") then

	elseif (node.id == "toggle_editor_win") then
		data.__info.ui_editor_win = p.active
	end
end

function editor_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_win") then
		local w,h = core.window.size()
		p.y = data.editor_toolbar.y+data.editor_toolbar.h+1
		p.h = h-p.y
	end
end

function editor_grid_configure_close(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_grid_configure_win") then
		data[node.id] = nil -- to reinit when shown again
	end
end