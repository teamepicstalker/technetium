function framework_run()
	CallbackSet("framework_load",framework_load)
end

function framework_load()
	core.interface.import.prefab("example01_examples","prefab/ui/examples.ui",{ directory="lua",filter={"lua"}, onSelect=on_select })
	core.interface.import.prefab("example01_filebrowser","prefab/ui/filebrowser.ui",{ directory="lua",filter={"lua"}, onSelect=on_select })
end