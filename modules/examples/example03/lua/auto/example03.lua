--[[
	Example 3 - Cubes

In this example I show three different ways to render cubes on screen. One is by setting up vertex and index buffers directly and using manually created
vertices and indices. The other is importing a Box.gltf model from a GLTF and using the model:draw method. The last is loading an entity prefab equipped with preset visual and spatial components.

The projection matrix is handled by a camera prefab but can also be set up manually.
--]]

local bgfx = require("ffi_bgfx")
function framework_run()
	CallbackSet("framework_update",framework_update)
	CallbackSet("framework_render",framework_render)
	CallbackSet("framework_load",framework_load)
	CallbackSet("framework_mousemoved",framework_mousemoved)
	CallbackSet("framework_wheelmoved",framework_wheelmoved)
	CallbackSet("framework_keyboard",framework_keyboard)
	CallbackSet("framework_resize",framework_resize)
end

local test_mdl = nil
local vdecl = nil
local vdecl_h = nil
local cube_verts = nil
local cube_idx = nil
local vbo = nil
local ibo = nil
function framework_load()
	local cam = core.camera("default3D","prefab/camera/default_perspective.lua")
	cam:position(0,0,20)

	test_mdl = core.import.model("models/CubeVertexColors.gltf")
	
	local ent = game.entity.load("prefab/entity/cube.ent")
	ent.component.spatial:position(-5,2,1)
	
	vdecl = ffi.new('bgfx_vertex_layout_t[1]')
	bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,3,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_COLOR0,4,bgfx.BGFX_ATTRIB_TYPE_UINT8,true,false)
	bgfx.bgfx_vertex_layout_end(vdecl)
	vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)
	
	cube_verts = ffi.new("float[32]",
		-1, -1,  1, 0xff000000,
         1, -1,  1, 0xff0000ff,
         1,  1,  1, 0xff00ff00,
        -1,  1,  1, 0xff00ffff,
        -1, -1, -1, 0xffff0000,
         1, -1, -1, 0xffff00ff,
         1,  1, -1, 0xffffff00,
        -1,  1, -1, 0xffffffff)
	cube_idx = ffi.new("uint16_t[36]",
		0, 1, 2,   2, 3, 0,  
		4, 5, 6,   6, 7, 4,  
		3, 2, 6,   6, 7, 3,  
		0, 1, 5,   5, 4, 0,  
		0, 3, 7,   7, 4, 0,  
		1, 2, 6,   6, 5, 1)
	
	local mem = bgfx.bgfx_copy(cube_verts,ffi.sizeof(cube_verts))
	vbo = bgfx.bgfx_create_vertex_buffer(mem,vdecl,bgfx.BGFX_BUFFER_NONE)
	
	local mem2 = bgfx.bgfx_copy(cube_idx,ffi.sizeof(cube_idx))
	ibo = bgfx.bgfx_create_index_buffer(mem2,bgfx.BGFX_BUFFER_NONE)
end

function framework_mousemoved(x,y,dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:mousemoved(x,y,dx,dy)
	end
end

function framework_wheelmoved(dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:wheelmoved(dx,dy)
	end
end

function framework_keyboard(key,aliases,scancode,action,mods)

end

function framework_update(dt,mx,my)
	for id, ent in pairs(game.entity.all()) do
		for name,t in pairs(ent.component) do
			if (t.update) then
				t:update(dt,mx,my)
			end
		end
	end
end

function framework_resize(w,h)
	bgfx.bgfx_set_view_rect(0,0,0,w,h)
end

local mdl_mtx = {ffi.new("mat4_t"):identity(),ffi.new("mat4_t"):identity()}
mdl_mtx[1].m[12] = 3
mdl_mtx[1].m[13] = 5

mdl_mtx[2].m[12] = -3
mdl_mtx[2].m[13] = 5

local itr = 0
function framework_render(winW,winH,fbW,fbH)
	local cam = core.camera.active("default3D")
	if not (cam) then
		return
	end
	
	cam:send(0)
	
	-- drawn manually
	mdl_mtx[1]:rotateXY(itr,itr/2)
	
	if itr < 180 then
		itr = itr + 0.01
	else
		itr = 0
	end
	
	bgfx.bgfx_set_transform(mdl_mtx[1],1)
	bgfx.bgfx_set_state(bgfx.BGFX_STATE_WRITE_MASK+bgfx.BGFX_STATE_DEPTH_TEST_LESS+bgfx.BGFX_STATE_MSAA,0)
	
	bgfx.bgfx_set_vertex_buffer(0,vbo,0,ffi.sizeof(cube_verts))
	bgfx.bgfx_set_index_buffer(ibo,0,ffi.sizeof(cube_idx))
	local shader_prog_h = core.renderer.shader.active("cubes")

	bgfx.bgfx_submit(0,shader_prog_h,0,bgfx.BGFX_DISCARD_ALL)
	
	--manually draw GLTF model
	mdl_mtx[2]:rotateXY(itr/2,-itr/2)
	bgfx.bgfx_set_transform(mdl_mtx[2],1)
	test_mdl:draw(0)
	
	-- draw entity
	local ent = game.entity.byID(1)
	ent.component.spatial.properties.transform:rotateXY(itr,-itr/2)
	ent.component.spatial:set_transform()
	ent.component.visual:draw(0)
end