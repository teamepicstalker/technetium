--[[ 
	Scene Management
	The currently loaded scene
	
	A scene is a collection of entites and a unit of a 'World'
--]]

function save(fullpath)
	local data = {}
	stateWrite(data)
	
	local old = filesystem.getWriteDirectory()
	filesystem.setWriteDirectory(filesystem.getBaseDirectory().."data\\")
	
	local str = "return "..print_table(data,true)
	filesystem.write(fullpath,str)
	
	filesystem.setWriteDirectory(old)
end 

function load(fullpath)
	local result = filesystem.loadfile(fullpath)
	stateRead(result)
end

-- To write scene's state to table, used by World
function stateWrite(data)
	game.entity.stateWrite(data)
end

-- To read scene's state from table, used by World
function stateRead(data)
	game.entity.stateRead(data)
end