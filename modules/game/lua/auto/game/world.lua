--[[ 
	World Management
	
	A world is a collection of scenes and their entities. This additional layers allows the travel of entities
	between scenes, if necessary. This is what a new game will load as it's default state and will update to persist
	as a game save.
--]]

data = {}

function save(fullpath)
	stateWrite(data)
	
	local old = filesystem.getWriteDirectory()
	filesystem.setWriteDirectory(filesystem.getBaseDirectory().."data\\")
	
	local str = "return "..print_table(data,true)
	filesystem.write(fullpath,str)
	
	filesystem.setWriteDirectory(old)
end 

function load(fullpath)
	local result = filesystem.loadfile(fullpath)
	stateRead(result)
end

-- To write world's state to table
function stateWrite(data)
	for name, path in pairs(data) do
		game.scene.stateWrite(data)
	end
end

-- To read world's state from table
function stateRead(data)
	for name, path in pairs(data) do
		game.scene.stateRead(data)
	end
end