--[[ 
	Entity Management

	Entities are containers of related components that may extend
	the properties and attributes of the entity
	
	TIPS:
	
	Never reference and store an entity long-term in any table, reference only in current scope. Instead track by id, when you
	need an entity, call 'byID'. This is so that when a scene changes or clears, entity can garbage collect.
--]]

_curID			= 0				-- ID of last created object
_entities		= {}			-- List of entities
_count			= 0				-- Entity count

--[[	clear
erases entities table and resets count and ID index
--]]
function clear()
	_curID			= 0
	_entities		= {}
	_count			= 0
end

--[[	forEach
execute a function for each and every entity
--]]
function forEach(funct,...)
	for id,ent in pairs(_entities) do
		funct(id,ent,...)
	end
end

--[[	forEachByScene
execute a function for each and every entity in a scene by name
--]]
function forEachByScene(funct,sceneName,...)
	for id,ent in pairs(_entities) do
		if (ent.scene == sceneName) then
			funct(id,ent,...)
		end
	end
end

--[[	all
returns the entity table
--]]
function all()
	return _entities
end

--[[	count
returns the current entity count
--]]
function count()
	return _count
end 

--[[	byID
returns the entity with given ID
--]]
function byID(id)
	return _entities[ id ]
end

--[[	create
Create an entity with given components by name
--]]
function create(...)
	local ent = cEntity()
	for i=1,select('#',...) do
		local name = select(i,...)
		ent:addComponent(name)
	end
	return ent
end

--[[	destroy
Destroy the entity by given ID
--]]
function destroy(id)
	local ent = _entities[ id ]
	_entities[ id ] = nil
	if (ent) then
		ent:onDestroy()
	end
end

--[[	stateRead
Triggers a "state" read for all entity components
--]]
function stateRead(data)
	for id,storage in pairs(data._entities) do
		local ent = cEntity(storage.id,storage.alias,storage.scene)
		for name,comp in pairs(storage.component) do
			ent:addComponent(name)
			if (ent.component[name].stateRead) then
				ent.component[name]:stateRead(storage.component[name])
			end
		end
	end
end

--[[	stateWrite
Triggers a "state" write for all entity components
--]]
function stateWrite(data)
	data._entities = {}
	for id,ent in pairs(_entities) do
		if (ent.component.persistable) then
			local t = { component = {} }
			t.alias = ent.alias
			t.scene = ent.scene
			for name,_ in pairs(ent.component) do
				t.component[name] = {}
				if (ent.component[name].stateWrite) then
					ent.component[name]:stateWrite(t.component[name])
				end
			end
			data._entities[id] = t
		end
	end	
end

--[[	generateID
Generates a new ID, which can be assigned to an entity
--]]
function generateID()
	_curID = _curID + 1
	return _curID
end

-- load from file
function load(fullpath)
	local str,len = filesystem.read(fullpath)
	if (len == nil or len <= 0) then
		return
	end
	local status,result = pcall(assert(loadstring(str,fullpath:sub(-45))))
	if not (status) then
		print("[ERROR] failed to import scene " .. fullpath .. "\n" .. result)
		return
	end
	local ent = cEntity(nil,result.alias,nil)
	for name,comp in pairs(result.component) do
		ent:addComponent(name)
		if (ent.component[name].stateRead) then
			ent.component[name]:stateRead(result.component[name])
		end
	end
	return ent
end 

-- save to file
function save(fullpath,ent)
	local data = { component = {} }
	data.alias = ent.alias
	data.scene = nil -- It's a prefab
	for name,_ in pairs(ent.component) do
		data.component[name] = {}
		if (ent.component[name].stateWrite) then
			ent.component[name]:stateWrite(data.component[name])
		end
	end

	local old = filesystem.getWriteDirectory()
	filesystem.setWriteDirectory(filesystem.getBaseDirectory().."data\\")
	
	local str = "return "..print_table(data,true)
	filesystem.write(fullpath,str)
	
	filesystem.setWriteDirectory(old)
end

--[[
	cEntity
--]]
Class "cEntity"
function cEntity:__init(id,alias,scene)
	_count = _count + 1
	self.id = id and not _entities[id] and id or generateID()
	self.alias = alias or "entity"..self.id
	self.scene = scene
	_entities[self.id] = self
	self.component = {}
end

function cEntity:addComponent(name)
	if (self.component[name] ~= nil) then
		return
	end
	local c = game.entity.component[name]
	if (c) then
		self.component[name] = c(self)
		if (self.component[name].dependencies) then
			for i,dependent in ipairs(self.component[name].dependencies) do
				self:addComponent(dependent)
			end
		end
	end
end

function cEntity:removeComponent(name)
	local c = self.component[name]
	if (c == nil) then 
		return
	end
	self.component[name] = nil
	for other,t in pairs(self.component) do
		if (t.dependencies) then 
			for i,dependent in ipairs(t.dependencies) do 
				if (dependent == name) then 
					self:removeComponent(other)
					break
				end
			end
		end
	end
end

function cEntity:component(name)
	return self.component[name]
end

function cEntity:onDestroy()
	for name,_ in pairs(self.component) do
		local c = game.entity.component[name]
		if (c and c.onDestroy) then 
			c.onDestroy(entity)
		end
	end
	_count = _count - 1
end

