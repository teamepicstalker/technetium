-- game.entity.component.spatial()
getmetatable(this).__call = function (self,entity)
	return cSpatialManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSpatialManager"
function cSpatialManager:__init(entity)
	self.entity = entity
	self.properties = { position=ffi.new("vec3_t"), transform=ffi.new("mat4_t"):identity(), angle=ffi.new("vec3_t") }
end

function cSpatialManager:transform()
	return self.properties.transform
end

function cSpatialManager:position(x,y,z)
	local pos = self.properties.position
	if (x or y or z) then
		pos.x = x or pos.x
		pos.y = y or pos.y
		pos.z = z or pos.z
	end
	return pos:unpack()
end

function cSpatialManager:angle(x,y,z)
	local ang = self.properties.angle
	ang(ang.x+x,ang.y+y,ang.z+z)
	ang.x = utils.wrap_minmax(ang.x,-2*math.pi,2*math.pi)
	ang.y = utils.wrap_minmax(ang.y,-2*math.pi,2*math.pi)
	ang.z = utils.wrap_minmax(ang.z,-2*math.pi,2*math.pi)
	return ang
end

function cSpatialManager:set_transform()
	local pos = self.properties.position
	local mdl_mtx = self.properties.transform
	local offset_mtx = mdl_mtx * mdl_mtx:translation(pos:unpack())
	core.renderer.set_transform(offset_mtx,1)
end

-------------------------------
-- persistance
-------------------------------
function cSpatialManager:stateWrite(data)
	local p = self.properties.transform
	data.transform = {p.m[0],p.m[1],p.m[2],p.m[3],p.m[4],p.m[5],p.m[6],p.m[7],p.m[8],p.m[9],p.m[10],p.m[11],p.m[12],p.m[13],p.m[14],p.m[15]}
	p = self.properties.angle
	data.angle = {p.x,p.y,p.z}
	p = self.properties.position
	data.position = {p.x,p.y,p.z}
end

function cSpatialManager:stateRead(data)
	self.properties.transform = data.transform and ffi.new("mat4_t",data.transform) or self.properties.transform
	self.properties.angle = data.angle and ffi.new("vec3_t",data.angle) or self.properties.angle
	self.properties.position = data.position and ffi.new("vec3_t",data.position) or self.properties.position	
end