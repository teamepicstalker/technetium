-- game.entity.component.sprite()
getmetatable(this).__call = function (self,entity)
	return cSprite(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSprite"
function cSprite:__init(entity)
	self.dependencies = {"spatial"}
	self.entity = entity
	self.properties = { visible=true, animate=true, image=nil, layer=nil }
end

function cSprite:image(path)
	if (path == nil) then
		return self.properties.image
	end
	if (self.properties.image and self.properties.image.name == path) then 
		return self.properties.image
	end
	local image_import = core.import.image
	local image = path ~= "" and (image_import.animation.exists(path) and image_import.animation(path,false) or image_import.exists(path) and image_import(path))
	if (image) then
		self.properties.image = image
		if (image.reset) then
			image:reset()
		end
	else
		self.properties.image = nil
	end
	return self.properties.image
end

function cSprite:update(dt,mx,my)
	-- if (self.properties.animate and self.properties.image and self.properties.image.image_animation_update) then
		-- self.properties.image:image_animation_update(dt,mx,my)
	-- end
end

function cSprite:draw()
	if (self.properties.visible ~= true or self.properties.image == nil) then 
		return
	end
	self.entity.component.spatial:set_transform()
	self.properties.image:draw()
end

function cSprite:getTextureInfo()
	if (self.properties.image == nil) then
		return
	end
	return self.properties.image:getTextureInfo()
end

function cSprite:getTextureID()
	return self.properties.image ~= nil and self.properties.image:getTextureID() or core.import.image.cache.default.id
end

function cSprite:getRegion()
	if (self.properties.image == nil) then 
		return 0,0,0,0
	end
	return self.properties.image:getRegion()
end

function cSprite:getTextureSize()
	if (self.properties.image == nil) then 
		return 0,0
	end
	return self.properties.image:getTextureSize()
end

function cSprite:stateWrite(data)
	data.path = self.properties.image and self.properties.image.name or nil
end 

function cSprite:stateRead(data)
	self:image(data.path)
end